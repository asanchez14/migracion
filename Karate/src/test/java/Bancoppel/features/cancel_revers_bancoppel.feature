Feature: Cancel / Revers

  Background:

    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_cancel_policies')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/Bancoppel/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodCancel')
    * def RequestBody = read(userDir + source + 'requestBodyCancelar.json')
#    * def RequestBody1 = read(userDir + source + 'requestBodyCancelPolicies.json')


  Scenario: Datos correctos cancelación
    Given path 'v1/cancel_policy'
#    * def RequestBody = read(userDir + source + 'requestBodyCancelar.json')
    And request RequestBody
    When method put
    Then status 200
    And print 'Response is: ', response

  Scenario: No. Póliza con número de certificado responde code 043-PT
    Given path 'v1/cancel_policy'
   * def javaMethod = new javaClass().setNoPoliciesConCertificate()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "043-PT"

  Scenario: No Police en blanco responde code 013-PO
    Given path 'v1/cancel_policy'
    * def javaMethod = new javaClass().setNoPoliciesBlanco()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "013-PO"

  Scenario: No. Póliza con caracteres especiales y una logintud de más de 35 caracteres responde code 043-PT,033-PL
    Given path 'v1/cancel_policy'
    * def javaMethod = new javaClass().setNoPoliciesLargoCharacterEspecial()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "043-PT"

  Scenario: No. Póliza no existe responde code 004-PN
    Given path 'v1/cancel_policy'
    * def javaMethod = new javaClass().setNoPoliciesNoExist()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "004-PN"

      ##### NO.SUCURSAL #####
  Scenario: No. Sucursal en blanco responde code 010-PO
    Given path 'v1/cancel_policy'
    * def javaMethod = new javaClass().setNoSubsidiaryBlanco()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "010-PO"

  Scenario: No. Sucursal con caracteres especiales y más de 10 caracteres responde code 015-PT,009-PL
    Given path 'v1/cancel_policy'
    * def javaMethod = new javaClass().setNoSubsidiaryLargoCharacterEspecial()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "015-PT,009-PL"

    ##### ID USUARIO #####
  Scenario: ID Usuario con caracteres especiales y más de 10 caracteres responde code "035-PT,009-PL"
    Given path 'v1/cancel_policy'
    * def javaMethod = new javaClass().setIdUserLargoCharactersEspecial()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "035-PT,009-PL"

  Scenario: ID Usuario en blanco responde code "021-PO"
    Given path 'v1/cancel_policy'
    * def javaMethod = new javaClass().setIdUserBlanco()
    And def RequestBody = read(userDir + source + 'requestBodyBadCancel.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.cancelPolicyResponse.codigoError contains "021-PO"


    #### CANCELACIÓN DE PÓLIZAS #####
  Scenario: Cancelar 1 póliza con 1 migrante asegurado
    Given path 'v1/cancel_policy'
    And def RequestBody = read(userDir + source + 'requestBodyCancelPolicies.json')
    And request RequestBody[0]
    When method put
    Then status 200
    And print 'Response is: ', response

  Scenario: Cancelar 1 póliza con 2 migrantes asegurados
    Given path 'v1/cancel_policy'
    And def RequestBody = read(userDir + source + 'requestBodyCancelPolicies.json')
    And request RequestBody[1]
    When method put
    Then status 200
    And print 'Response is: ', response

#  Scenario : Cancelar 1 póliza con 3 migrantes asegurados
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[2]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#
#  Scenario : Cancelar 1 póliza con 4 migrantes asegurados
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[3]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#
#  Scenario : Cancelar 1 póliza con 5 migrantes asegurados
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[4]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#
#  Scenario : Cancelar 1 póliza con 6 migrantes asegurados
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[5]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#
#  Scenario : Cancelar 1 póliza con vigencia de 6 meses
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[6]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#
#
#    ##### CANCELAR PÓLIZA RENOVADA EL MISMO DÍA ANTES DE FIN DE VIGENCIA #####
#  Scenario : Una póliza que se encontraba en periodo de carencia 45 días antes de fecha vencimiento es renovada y después cancelar
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[7]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#
#  ##### CANCELAR PÓLIZA RENOVADA EL MISMO DÍA DESPUÉS DE FIN DE VIGENCIA #####
#  Scenario : Una póliza que se encontraba en periodo de carencia 30 días después de fecha vencimiento es renovada y después cancelada
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[8]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#
#     ##### CANCELACIÓN DE PÓLIZA 2 VECES #####
#  Scenario : Cancelar una póliza 2 veces responde code "026-PN"
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[9]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#    And match response.RetrievePolicyReponse.CodResp contains "026-PN"
#
#  ##### CANCELACIÓN DE PÓLIZA VENDIDA 1 DÍA ANTES DEL ACTUAL #####
#  Scenario : Cancelar una póliza que se vendio un día antes responde code "058-PN"
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[10]
#    When method post
#    Then status 200
#    And print 'Response is: ', response
#    And match response.RetrievePolicyReponse.CodResp contains "058-PN"
#
#    ##### CANCELACIÓN DE PÓLIZA CON 10 MIGRANTES #####
#  Scenario : Cancelar póliza de asegura migrante número 10
#    Given path 'v1/cancel_policy'
#    And request RequestBody1[11]
#    When method post
#    Then status 200
#    And print 'Response is: ', response



