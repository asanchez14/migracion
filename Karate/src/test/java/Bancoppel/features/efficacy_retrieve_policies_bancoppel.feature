Feature: API Efficacy / Retrieve


  Background:

       ######## Escenarios multiples  ########
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_eficacia_policies')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/Bancoppel/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodEffectiveRetrieve')



     ######## DATOS CORRECTOS ########
  Scenario: Validacion todos los datos correctos
    Given path 'sales/policies/retrieve'
    * def bodyRequest = read(userDir + source + 'requestBodyEffectiveRetrieve.json')
    And request bodyRequest[0]
    When method post
    Then status 200
    Then print "Validate HTTP Code 200 response: ", response
    #response of validation(json)
    And match response.[*].NumeroPoliza == "#notnull"
    And match response.[*].PersonNombre == "#string", "#notnull"
#    And match response.[*].PersonSegundoNombre == "#string", "#notnull"
#    And match response.[*].PersonApellidoPaterno == "#string", "#notnull"
#    And match response.[*].PersonApellidoMaterno == "#notnull", "#notnull"
#    And match response.[*].PersonFechaNacimiento == "#string", "#notnull"

    ##### CERTIFICADO #####
  Scenario: Valida code 039-PN campo certificado con caracteres especiales y logitud de más de 30 caracteres
    Given path 'sales/policies/retrieve'
    * def javaMethod = new javaClass().setCertificationLarge()
    And def bodyRequest = read(userDir + source + 'requestBodyEfficacyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
#    And match response.RetrievePolicyReponse.MsjTransaccion contains "ERROR"
    And match response.RetrievePolicyReponse.CodResp contains "023-PL,012-PT"


  ##### PRIMER NOMBRE ######
  Scenario: Primer nombre correcto, compuesto, con letra ñ y acento
    Given path 'sales/policies/retrieve'
    * def bodyRequest = read(userDir + source + 'requestBodyEffectiveRetrieve.json')
    And request bodyRequest[0]
    When method post
    Then status 200
    Then print "Validate HTTP Code 200 response: ", response
    #response of validation(json)
    And match response.[*].NumeroPoliza == "#notnull"
    And match response.[*].PersonNombre == "#string", "#notnull"
#    And match response.[*].PersonSegundoNombre == "#string", "#notnull"
#    And match response.[*].PersonApellidoPaterno == "#string", "#notnull"
#    And match response.[*].PersonApellidoMaterno == "#string", "#notnull"
#    And match response.[*].PersonFechaNacimiento == "#string", "#notnull"
#
#
#
  Scenario: Valida code 022-PL y 011-PT campo primerNombre com más de 26 caracteres y caracteres especiales
    Given path 'sales/policies/retrieve'
    * def javaMethod = new javaClass().setPrimerNombreLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyEfficacyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
    And match response.RetrievePolicyReponse.CodResp contains "022-PL,011-PT"
#
#
  Scenario: Valida code 020-PO Campo primer nombre en blanco
    Given path 'sales/policies/retrieve'
    * def javaMethod = new javaClass().setPrimerNombreBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyEfficacyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
    And match response.RetrievePolicyReponse.CodResp contains "020-PO"
#
#
#            ##### SEGUNDO NOMBRE ######
#  Scenario: Segundo nombre correcto, compuesto, con letra ñ y acento
#    Given path 'sales/policies/retrieve'
#    And request bodyRequest[1]
#    When method post
#    Then status 200
#    Then print "Validate HTTP Code 200 response: ", response
#    #response of validation(json)
#    And match response.[*].NumeroCertificado == "#notnull"
#    And match response.[*].PersonPrimerNombre == "#string", "#notnull"
#    And match response.[*].PersonSegundoNombre == "#string", "#notnull"
#    And match response.[*].PersonApellidoPaterno == "#string", "#notnull"
#    And match response.[*].PersonApellidoMaterno == "#string", "#notnull"
#    And match response.[*].PersonFechaNacimiento == "#string", "#notnull"
#
#
#  Scenario: Valida code 018-PL y 023-PT campo segundoNombre com más de 26 caracteres y caracteres especiales
#    Given path 'sales/policies/retrieve'
#    * def javaMethod = new javaClass().setSegundoNombreLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method post
#    Then status 400
#    Then print "Validate HTTP Code 400 response: ", response
#    And match response.RetrievePolicyReponse.CodResp contains "018-PL,023-PT"
#
#
#  Scenario: Valida campo segundo nombre en blanco
#    Given path 'sales/policies/retrieve'
#    * def javaMethod = new javaClass().setSegundoNombreBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method post
#    Then status 200
#    Then print "Validate HTTP Code 200 response: ", response
#
#
        ##### APELLIDO PATERNO ######
  Scenario: Apellido paterno correcto, compuesto, con letra ñ y acento
    Given path 'sales/policies/retrieve'
    * def bodyRequest = read(userDir + source + 'requestBodyEffectiveRetrieve.json')
    And request bodyRequest[1]
    When method post
    Then status 200
    Then print "Validate HTTP Code 200 response: ", response
#    #response of validation(json)
    And match response.[*].NumeroPoliza == "#notnull"
    And match response.[*].PersonNombre == "#string", "#notnull"
#    And match response.[*].PersonSegundoNombre == "#string", "#notnull"
    And match response.[*].PersonApellidoPaterno == "#string", "#notnull"
#    And match response.[*].PersonApellidoMaterno == "#string", "#notnull"
#    And match response.[*].PersonFechaNacimiento == "#string", "#notnull"
#
#
  Scenario: Valida code 002-PL y 002-PT campo apellido paterno com más de 26 caracteres y caracteres especiales
    Given path 'sales/policies/retrieve'
    * def javaMethod = new javaClass().setApellidoPaternoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyEfficacyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
    And match response.RetrievePolicyReponse.CodResp contains "002-PL,002-PT"
#
#
  Scenario: Valida code 002-PO campo apellido paterno en blanco
    Given path 'sales/policies/retrieve'
    * def javaMethod = new javaClass().setApellidoPaternoNombreBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyEfficacyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
    And match response.RetrievePolicyReponse.CodResp contains "002-PO"
#
#      ##### APELLIDO MATERNO ######
#  Scenario: Apellido materno correcto, compuesto, con letra ñ y acento
#    Given path 'sales/policies/retrieve'
#    And request bodyRequest[0]
#    When method post
#    Then status 200
#    Then print "Validate HTTP Code 200 response: ", response
#     #response of validation(json)
#    And match response.[*].NumeroCertificado == "#string", "#notnull"
#    And match response.[*].PersonPrimerNombre == "#string", "#notnull"
#    And match response.[*].PersonSegundoNombre == "#string", "#notnull"
#    And match response.[*].PersonApellidoPaterno == "#string", "#notnull"
#    And match response.[*].PersonApellidoMaterno == "#string", "#notnull"
#    And match response.[*].PersonFechaNacimiento == "#string", "#notnull"
#
#  Scenario: Valida code 001-PL y 001-PT campo apellido materno com más de 26 caracteres y caracteres especiales
#    Given path 'sales/policies/retrieve'
#    * def javaMethod = new javaClass().setApellidoMaternoLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest[12]
#    When method post
#    Then status 400
#    Then print "Validate HTTP Code 400 response: ", response
#    And match response.RetrievePolicyReponse.CodResp contains "001-PL,001-PT"
#
#  Scenario: Valida campo apellido materno en blanco
#    Given path 'sales/policies/retrieve'
#    * def javaMethod = new javaClass().setApellidoMaternoNombreBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest[13]
#    When method post
#    Then status 200
#    Then print "Validate HTTP Code 200 response: ", response
#
#
    ##### FECHA DE NACIMIENTO ######
  Scenario: Fecha de nacimiento correcto
    Given path 'sales/policies/retrieve'
    * def bodyRequest = read(userDir + source + 'requestBodyEffectiveRetrieve.json')
    And request bodyRequest[0]
    When method post
    Then status 200
    Then print "Validate HTTP Code 200 response: ", response
#     #response of validation(json)
     And match response.[*].NumeroPoliza == "#notnull"
    And match response.[*].PersonNombre == "#string", "#notnull"
#    And match response.[*].PersonSegundoNombre == "#string", "#notnull"
    And match response.[*].PersonApellidoPaterno == "#string", "#notnull"
#    And match response.[*].PersonApellidoMaterno == "#string", "#notnull"
    And match response.[*].PersonFechaNacimiento == "#string", "#notnull"
#
  Scenario: Valida code 019-PL y 026-PT campo fecha de nacimiento com más de 10 caracteres y formato incorrecto
    Given path 'sales/policies/retrieve'
    * def javaMethod = new javaClass().setFechaNacimientoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyEfficacyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
    And match response.RetrievePolicyReponse.CodResp contains "019-PL,026-PT"
#
  Scenario: Valida code 012-PO campo fecha de nacimiento en blanco
    Given path 'sales/policies/retrieve'
    * def javaMethod = new javaClass().setFechaNacimientoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyEfficacyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
    And match response.RetrievePolicyReponse.CodResp contains "012-PO"
#
    ##### MULTIPLES CONTRATOS ######
  Scenario: Valida multiples contratos
    Given path 'sales/policies/retrieve'
    * def bodyRequest = read(userDir + source + 'requestBodyEffectiveRetrieve.json')
    And request bodyRequest[2]
    When method post
    Then status 400
    Then print "Validate HTTP Code 400 response: ", response
    And match response.RetrievePolicyReponse.CodResp contains "040-PN"
#
##    ##### Consulta de 1 migrante sin actualización de datos #####
##  Scenario: Consulta de datos 1 migrante sinactualización de datos
##    Given path 'Cardif/sales/policies/retrieve'
##    And request bodyRequest[18]
##    When method post
##    Then status 200
##    Then print "Validate HTTP Code 200 response: ", response
##    #response of validation(json)
##    And match response == expectedOutput[0]
##    And match response.RetrievePolicyReponse.PolizaArray.[1].Person.[0].PersonFechaNacimiento == " "
##    And match response.RetrievePolicyReponse.PolizaArray.[1].Person.[0].PersonRfcHomoclave == "No Aplica"
##
##
#    ##### VALIDACIÓN CAMPOS RESPONSE BODY #####
#  Scenario: Consulta de datos
#    Given path 'sales/policies/retrieve'
#    And request bodyRequest[0]
#    When method post
#    Then status 200
#    Then print "Validate HTTP Code 200 response: ", response
#     #response of validation(json)
#    #And match response == expectedOutput[1]
#    And match response.RetrievePolicyReponse.PolizaArray[0].NumeroCertificado == "02020043940FF0"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].NumeroPoliza == "22772306447G609"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].EstatusPoliza == "ALTA DEL PRODUCTO"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Asistenciadora == "IKE"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonPrimerNombre == "J Guadalupe"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonApellidoPaterno == "López"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonApellidoMaterno == "Leonardo"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonSexo == "F"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonFechaNacimiento == "03/01/1960"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonEstadoNacimiento == 74
#                                                                                                   #11
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonNacionalidad == 74
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonCalle == "Estrella"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonNumeroExterior == "18"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonNumeroInterior == "1B"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonCP == "55339"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonColonia == "1"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonDelegacionMunicipio == "1"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonCiudadEstado == "15"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonRfcHomoclave == "LOVL880506J61"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonTelefonoCelular == "5552171519"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[0].PersonEmail == "cuenta@dominio.com"
##    And match response.RetrievePolicyReponse.PolizaArray.[1].NumeroCertificado == "NumeroCertificado"
##    And match response.RetrievePolicyReponse.PolizaArray.[1].NumeroPoliza == "NumeroPoliza"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].FechaInicioSeguro == "22/09/2020"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].FechaFinSeguro == "22/02/2021"
##    And match response.RetrievePolicyReponse.PolizaArray.[1].PeriodoPago == "PeriodoPago"
##    And match response.RetrievePolicyReponse.PolizaArray.[1].PrimaPoliza == "PrimaPoliza"
##    And match response.RetrievePolicyReponse.PolizaArray.[1].sumaAsegurada == "sumaAsegurada"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonPrimerNombre == "Lizeth Toña de Jesús"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonApellidoPaterno == "López"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonApellidoMaterno == "Vázquez de Riaño"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonSexo == "F"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonFechaNacimiento == "03/11/1986"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonEstadoNacimiento == 1
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonRfcHomoclave == "PERD85031814"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonEmail == "cuenta@dominio.com"
#                                                                                 #deberia estar vacio el email
##    And match response.RetrievePolicyReponse.PolizaArray.[1].Person.[0].PersonTypeId == "2"
#    And match response.RetrievePolicyReponse.PolizaArray.[0].Person.[1].PersonParentescoId == 0
