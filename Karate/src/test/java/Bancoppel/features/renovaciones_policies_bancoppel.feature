Feature: Renovaciones

  Background:

    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_renovaciones_policies')

    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/Bancoppel/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodRenewal')
    * def RequestBody = read(userDir + source + 'requestBodyRenewal.json')
  #  * def RequestBody1 = read(userDir + source + 'requestBodyUpdateMigrante.json')


  Scenario: Datos correctos renovación
    Given path 'v1/renewal_policy'
    And request RequestBody[0]
    When method post
    Then status 200
    And print 'Response is: ', response
#    And match response.updateMultiPolicy.NoSucursal == "99"
#    And match response.updateMultiPolicy.IdUsuario == "UO0020"
#    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "José"
#    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == "de Jesús"
#    And match response.updateMultiPolicy.person.[0].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[0].PersonFechaNacimiento == "17/06/1992"
#    And match response.updateMultiPolicy.person.[0].PersonTelefonoCelular == "5617345208"
#    And match response.updateMultiPolicy.person.[0].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817"
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonTelefonoCelular == "5617345208"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.FechaInicioSeguro == "05.0102084382084864QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.FechaFinSeguro == "05.0102084382084864QA1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PeriodoPago == "4"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PrimaPoliza == "240"


    ##### NO. SUCURSAL #####
  Scenario: No. Sucursal con caracteres especiales, letras y más de 10 caracteres responde code "015-PT,009-PL"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setNumeroSucursalLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.renewalPolicy.CodResp contains "015-PT,009-PL"

  Scenario : No. Sucursal en blanco responde code "010-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setNumeroSucursalBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "010-PO"

      ##### ID USUARIO #####
  Scenario : Id usuario con caracteres especiales y más de 50 caracteres responde code "035-PT" y "024-PL"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setIdUsuarioLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "035-PT,024-PL"

  Scenario : Id usuario en blanco responde code "021-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setIdUsuarioBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "021-PO"

    ##### PRIMER NOMBRE ASEGURADO #####
  Scenario : Primer nombre del asegurado con caracteres especiales y más de 26 caracteres responde code "022-PL" y "011-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPrimerNombreLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "022-PL,011-PT"

  Scenario : Primer nombre del asegurado en blanco responde code "020-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPrimerNombreBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "020-PO"

  Scenario : Primer nombre del asegurado con acento y letra ñ
    Given path 'v1/update_policy'
    And request RequestBody[1]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "José Toño"


    ##### SEGUNDO NOMBRE ASEGURADO #####
  Scenario : Segundo nombre del asegurado con caracteres especiales y más de 26 caracteres responde code "018-PL y 023-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setSegundoNombreLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "018-PL,023-PT"

  Scenario : Segundo nombre del asegurado en blanco es un campo opcional
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setSegudnoNombreBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == ""

  Scenario : Segundo nombre del asegurado con acento y letra ñ
    Given path 'v1/update_policy'
    And request RequestBody[2]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "José Toño"


    ##### APELLIDO PATERNO ASEGURADO #####
  Scenario : Apellido paterno del asegurado con caracteres especiales y más de 26 caracteres responde code "002-PL y 002-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoPaternoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "002-PL,002-PT"

  Scenario : Apellido paterno del asegurado en blanco responde code "002-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoPaternoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "002-PO"

  Scenario : Apellido paterno del asegurado con acento y letra ñ
    Given path 'v1/update_policy'
    And request RequestBody[3]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "Riaño-López"


     ##### APELLIDO MATERNO ASEGURADO #####
  Scenario : Apellido materno del asegurado con caracteres especiales y más de 26 caracteres responde code "001-PL y 001-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoMaternoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "001-PL,001-PT"

  Scenario : Apellido materno del asegurado en blanco es un campo opcional
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoMaternoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == ""

  Scenario : Apellido materno del asegurado con acento y letra ñ
    Given path 'v1/update_policy'
    And request RequestBody[4]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "Riaño-López"


       ##### FECHA DE NACIMIENTO ASEGURADO #####
  Scenario : Fecha nacimiento del asegurado con formato incorrecto empezando por año y más de 10 caracteres responde code "019-PL,026-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoLargoFormatoIncorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "019-PL,026-PT"

  Scenario : Fecha nacimiento del asegurado en blanco no es un campo obligatorio
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "019-PL,026-PT"

  Scenario : Fecha de nacimineot del asegurado con fecha mayor a la actual responde code "054-PN"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMayorActual()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "054-PN"

      ##### CONTRATANTE ASEGURADO MAYOR DE 75 AÑOS #####
  Scenario : Actualizar la fecha de nacimiento del asegurado mayor a 75 años responde code "014-PN"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMayorPermitida()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "014-PN"


    ##### CONTRATANTE ASEGURADO MENOR DE 18 AÑOS #####
  Scenario : Actualizar la fecha de nacimiento del asegurado menor a 18 años responde code "015-PN"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMenorPermitida()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "015-PN"


     ##### TELÉFONO ASEGURADO #####
  Scenario : Teléfono del asegurado con caracteres especiales y letras responde code "022-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoCaracteresEspecialesLetras()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "022-PT"

  Scenario : Teléfono del asegurado en blanco no es un campo obligatorio
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonEstadoNacimiento == ""

  Scenario : Teléfono del asegurado con menos de 8 digitos responde code "015-PL"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoMenosDigitos()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "015-PL"

  Scenario : Teléfono del asegurado con más de 30 digitos responde code "015-PL"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoMasDigitos()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "015-PL"


      ##### NUMERO DE CERTIFICADO ASEGURADO #####
  Scenario : Numero de certificado del asegurado con caracteres especiales y más de 30 caracteres responde code "023-PL,012-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setCertificadoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "023-PL,012-PT"

  Scenario : Numero de certificado del asegurado en blanco es un campo obligatorio responde code "008-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setCertificadoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "008-PO"


    ############ DATOS MIGRABTE ############

      ##### PRIMER NOMBRE ASEGURADO MIGRANTE #####
  Scenario : Primer nombre del asegurado migrante con caracteres especiales y más de 26 caracteres responde code "022-PL" y "011-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPrimerNombreMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "022-PL,011-PT"

  Scenario : Primer nombre del asegurado migrante en blanco responde code "020-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPrimerNombreMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "020-PO"

  Scenario : Primer nombre del asegurado migrante con acento y letra ñ
    Given path 'v1/update_policy'
    And request RequestBody[5]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "Jesús-Toño"


     ##### SEGUNDO NOMBRE ASEGURADO MIGRANTE #####
  Scenario : Segundo nombre del asegurado migrante con caracteres especiales y más de 26 caracteres responde code "018-PL y 023-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setSegundoNombreMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "018-PL,023-PT"

  Scenario : Segundo nombre del asegurado en blanco es un campo opcional
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setSegundoNombreMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == ""

  Scenario : Segundo nombre del asegurado con acento y letra ñ
    Given path 'v1/update_policy'
    And request RequestBody[6]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == "Toño de Jesús"


    ##### APELLIDO PATERNO ASEGURADO MIRANTE#####
  Scenario : Apellido paterno del asegurado migrante con caracteres especiales y más de 26 caracteres responde code "002-PL y 002-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoPaternoMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "002-PL,002-PT"

  Scenario : Apellido paterno del asegurado en blanco responde code "002-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoPaternoMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "002-PO"

  Scenario : Apellido paterno del asegurado con acento y letra ñ
    Given path 'v1/renewal_policy'
    And request RequestBody[7]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Riaño-López"


     ##### APELLIDO MATERNO ASEGURADO #####
  Scenario : Apellido materno del asegurado con caracteres especiales y más de 26 caracteres responde code "001-PL y 001-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoMaternoMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "001-PL,001-PT"

  Scenario : Apellido materno del asegurado en blanco es un campo opcional
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setApellidoMaternoMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == ""

  Scenario : Apellido materno del asegurado con acento y letra ñ
    Given path 'v1/renewal_policy'
    And request RequestBody[8]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == "Riaño-López"


       ##### FECHA DE NACIMIENTO ASEGURADO MIGRANTE #####
  Scenario : Fecha nacimiento del asegurado migrante con formato incorrecto empezando por año y más de 10 caracteres responde code "019-PL,026-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMigranteLargoFormatoIncorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "019-PL,026-PT"

  Scenario : Fecha nacimiento del asegurado migrante en blanco no es un campo obligatorio
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonFechaNacimiento == ""

  Scenario : Fecha de nacimineot del asegurado migrante con fecha mayor a la actual responde code "054-PN"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMigranteMayorActual()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "054-PN"

      ##### ASEGURADO MIGRANTE MAYOR DE 75 AÑOS #####
  Scenario : Actualizar la fecha de nacimiento del asegurado migrante mayor a 75 años responde code "014-PN"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMigranteMayorPermitida()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "014-PN"


    ##### ASEGURADO MIGRANTE MENOR DE 18 AÑOS #####
  Scenario : Actualizar la fecha de nacimiento del asegurado migrante menor a 18 años responde code "015-PN"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaNacimientoMigranteMenorPermitida()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "015-PN"


  ##### TELÉFONO ASEGURADO MIGRANTE #####
  Scenario : Teléfono del asegurado migrante con caracteres especiales y letras responde code "022-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoMigranteCaracteresEspecialesLetras()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "022-PT"

  Scenario : Teléfono del asegurado migrante en blanco no es un campo obligatorio
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].PersonEstadoNacimiento == ""

  Scenario : Teléfono del asegurado migrante con menos de 8 digitos responde code "015-PL"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoMigranteMenosDigitos()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "015-PL"

  Scenario : Teléfono del asegurado migrante con más de 30 digitos responde code "015-PL"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setTelefonoMigranteMasDigitos()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "015-PL"


    ##### NUMERO DE CERTIFICADO ASEGURADO MIGRANTE #####
  Scenario : Numero de certificado del asegurado migrante con caracteres especiales y más de 30 caracteres responde code "023-PL,012-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setCertificadoMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "023-PL,012-PT"

  Scenario : Numero de certificado del asegurado migrante en blanco es un campo obligatorio responde code "008-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setCertificadoMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "008-PO"

      ##### FECHA INICIO SEGURO #####
  Scenario : Fecha inicio seguro con formato icnorrecto y más de 10 caracteres responde code "020-PL,010-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaInicioSeguroLargoFormatoIncorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "020-PL,010-PT"

  Scenario : Fecha inicio seguro en blanco responde code "017-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaInicioSeguroBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "017-PO"

  Scenario : Fecha inicio seguro mayor a la actual responde code "
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaInicioSeguroMayorActual()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "017-PO"

   ##### FECHA FIN SEGURO #####
  Scenario : Fecha fin seguro con formato icnorrecto y más de 10 caracteres responde code "008-PL,014-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaFinSeguroLargoFormatoIncorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "008-PL,014-PT"

  Scenario : Fecha fin seguro en blanco responde code "019-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaFinSeguroBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "019-PO"

  Scenario : Fecha fin seguro mayor a la actual responde code "
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setFechaFinSeguroMayorActual()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "017-PO"


      ##### PERIODO DE PAGO #####
  Scenario : Periodo de pago anual (4)
    Given path 'v1/update_policy'
    And request RequestBody[9]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].renewalPeriod == "4"

  Scenario : Periodo de pago semestral (5)
    Given path 'v1/update_policy'
    And request RequestBody[10]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[0].renewalPeriod == "5"

  Scenario : Periodo de pago en blanco es un campo obligatorio responde code "014-PO"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPeriodoPagoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "014-PO"

  Scenario : Periodo de pago con letras y caracteres especiales responde code "020-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPeriodoPagoLetrasCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "020-PT"

  Scenario : Periodo de pago con más de 2 caracteres responde code "013-PL"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPeriodoPagoLargo()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "013-PL"

  Scenario : Periodo de pago no registrado responde code "021-PT"
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPeriodoPagoNoRegistrado()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "021-PT"


     ##### PRIMA PÓLIZA #####
  Scenario : Prima póliza no correspondiente al periodo de pago responde code ""
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPrimaPolizaIncorrecta()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains ""

  Scenario : Prima póliza del asegurado migrante en blanco no es un campo obligatorio
    Given path 'v1/renewal_policy'
    * def javaMethod = new javaClass().setPrimaPolizaBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadRenewal.json')
    And request RequestBody
    When method post
    Then status 400
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[1].PrimaPoliza == ""


     ##### RENOVACIÓN PÓLIZA EN PERIODO DE GRACIA (45 DIAS ANTES DE FIN DE VIGENCIA) #####
  Scenario : Renovación póliza 45 días antes de fin de vigencia
    Given path 'v1/renewal_policy'
    And request RequestBody[11]
    When method post
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"



    ##### RENOVACIÓN PÓLIZA EN PERIODO DE GRACIA (30 DIAS DESPUÉS DE FIN DE VIGENCIA) #####
  Scenario : Renovación póliza 30 días después de fin de vigencia
    Given path 'v1/renewal_policy'
    And request RequestBody[12]
    When method post
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"


    ##### RENOVACIÓN PÓLIZA (46 DIAS ANTES DE FIN DE VIGENCIA) #####
  Scenario : Renovación póliza 46 días antes de fin de vigencia responde code ""
    Given path 'v1/renewal_policy'
    And request RequestBody[13]
    When method post
    Then status 200
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "021-PT"

    ##### RENOVACIÓN PÓLIZA (31 DIAS DESPUÉS DE FIN DE VIGENCIA) #####
  Scenario : Renovación póliza 31 días después de fin de vigencia
    Given path 'v1/renewal_policy'
    And request RequestBody[14]
    When method post
    Then status 200
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "021-PT"


    ##### RENOVACIÓN DE PÓLIZA 2 VECES EN UN DÍA #####
  Scenario : Renovación de póliza 2 veces en el mismo día
    Given path 'v1/renewal_policy'
    And request RequestBody[0]
    When method post
    Then status 200
    And print 'Response is: ', response
    And match response.RetrievePolicyReponse.CodResp contains "021-PT"


           ##### RENOVACIÓN DE PÓLIZA DE 6 ASEGURADOS MIGRANTES #####
  Scenario : Renovación de póliza con 6 asegurados migrantes correcto
    Given path 'v1/update_policy'
    And request RequestBody1[15]
    When method put
    Then status 200
    And print 'Response is: ', response
    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
    ########## SEGUNDO ASEGURADO ##########
    And match response.updateMultiPolicy.person.[2].PersonPrimerNombre == "OSCAR"
    And match response.updateMultiPolicy.person.[2].PersonSegundoNombre == "LUIS"
    And match response.updateMultiPolicy.person.[2].PersonApellidoPaterno == "JUAREZ"
    And match response.updateMultiPolicy.person.[2].PersonApellidoMaterno == "RAMIREZ"
    And match response.updateMultiPolicy.person.[2].PersonSexo == "M"
    And match response.updateMultiPolicy.person.[2].PersonFechaNacimiento == "10/07/1993"
    And match response.updateMultiPolicy.person.[2].PersonNacionalidad == "74"
    And match response.updateMultiPolicy.person.[2].PersonCiudadEstado == "ORLANDO"
    And match response.updateMultiPolicy.person.[2].PersonRfcHomoclave == "PEPE801206"
    And match response.updateMultiPolicy.person.[2].PersonCurp == "PEPE801206HDFNLG06"
    And match response.updateMultiPolicy.person.[2].PersonParentescoId == "3"
    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-2"
    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-2"
    ########## TERCER ASEGURADO ##########
    And match response.updateMultiPolicy.person.[3].PersonPrimerNombre == "OSCAR"
    And match response.updateMultiPolicy.person.[3].PersonSegundoNombre == "LUIS"
    And match response.updateMultiPolicy.person.[3].PersonApellidoPaterno == "JUAREZ"
    And match response.updateMultiPolicy.person.[3].PersonApellidoMaterno == "RAMIREZ"
    And match response.updateMultiPolicy.person.[3].PersonSexo == "M"
    And match response.updateMultiPolicy.person.[3].PersonFechaNacimiento == "10/07/1993"
    And match response.updateMultiPolicy.person.[3].PersonNacionalidad == "74"
    And match response.updateMultiPolicy.person.[3].PersonCiudadEstado == "ORLANDO"
    And match response.updateMultiPolicy.person.[3].PersonRfcHomoclave == "PEPE801206"
    And match response.updateMultiPolicy.person.[3].PersonCurp == "PEPE801206HDFNLG06"
    And match response.updateMultiPolicy.person.[3].PersonParentescoId == "3"
    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-3"
    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-3"
    ########## CUARTO ASEGURADO ##########
    And match response.updateMultiPolicy.person.[4].PersonPrimerNombre == "OSCAR"
    And match response.updateMultiPolicy.person.[4].PersonSegundoNombre == "LUIS"
    And match response.updateMultiPolicy.person.[4].PersonApellidoPaterno == "JUAREZ"
    And match response.updateMultiPolicy.person.[4].PersonApellidoMaterno == "RAMIREZ"
    And match response.updateMultiPolicy.person.[4].PersonSexo == "M"
    And match response.updateMultiPolicy.person.[4].PersonFechaNacimiento == "10/07/1993"
    And match response.updateMultiPolicy.person.[4].PersonNacionalidad == "74"
    And match response.updateMultiPolicy.person.[4].PersonCiudadEstado == "ORLANDO"
    And match response.updateMultiPolicy.person.[4].PersonRfcHomoclave == "PEPE801206"
    And match response.updateMultiPolicy.person.[4].PersonCurp == "PEPE801206HDFNLG06"
    And match response.updateMultiPolicy.person.[4].PersonParentescoId == "3"
    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-4"
    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-4"
    ########## QUINTO ASEGURADO ##########
    And match response.updateMultiPolicy.person.[5].PersonPrimerNombre == "OSCAR"
    And match response.updateMultiPolicy.person.[5].PersonSegundoNombre == "LUIS"
    And match response.updateMultiPolicy.person.[5].PersonApellidoPaterno == "JUAREZ"
    And match response.updateMultiPolicy.person.[5].PersonApellidoMaterno == "RAMIREZ"
    And match response.updateMultiPolicy.person.[5].PersonSexo == "M"
    And match response.updateMultiPolicy.person.[5].PersonFechaNacimiento == "10/07/1993"
    And match response.updateMultiPolicy.person.[5].PersonNacionalidad == "74"
    And match response.updateMultiPolicy.person.[5].PersonCiudadEstado == "ORLANDO"
    And match response.updateMultiPolicy.person.[5].PersonRfcHomoclave == "PEPE801206"
    And match response.updateMultiPolicy.person.[5].PersonCurp == "PEPE801206HDFNLG06"
    And match response.updateMultiPolicy.person.[5].PersonParentescoId == "3"
    And match response.updateMultiPolicy.person.[5].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-5"
    And match response.updateMultiPolicy.person.[5].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-5"
    ########## SEXTO ASEGURADO ##########
    And match response.updateMultiPolicy.person.[6].PersonPrimerNombre == "OSCAR"
    And match response.updateMultiPolicy.person.[6].PersonSegundoNombre == "LUIS"
    And match response.updateMultiPolicy.person.[6].PersonApellidoPaterno == "JUAREZ"
    And match response.updateMultiPolicy.person.[6].PersonApellidoMaterno == "RAMIREZ"
    And match response.updateMultiPolicy.person.[6].PersonSexo == "M"
    And match response.updateMultiPolicy.person.[6].PersonFechaNacimiento == "10/07/1993"
    And match response.updateMultiPolicy.person.[6].PersonNacionalidad == "74"
    And match response.updateMultiPolicy.person.[6].PersonCiudadEstado == "ORLANDO"
    And match response.updateMultiPolicy.person.[6].PersonRfcHomoclave == "PEPE801206"
    And match response.updateMultiPolicy.person.[6].PersonCurp == "PEPE801206HDFNLG06"
    And match response.updateMultiPolicy.person.[6].PersonParentescoId == "3"
    And match response.updateMultiPolicy.person.[6].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-6"
    And match response.updateMultiPolicy.person.[6].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-6"


