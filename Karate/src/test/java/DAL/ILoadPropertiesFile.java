package DAL;

import java.io.IOException;
import java.util.ArrayList;

public interface ILoadPropertiesFile {

    String getPropertiesLoad(String nameProp) throws IOException;
    ArrayList<String> getAllProperties();

}
