package DAL;

import utils.Constants;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

public class LoadPropertiesFile implements ILoadPropertiesFile{
    InputStream inputStream;
    String path = System.getProperty(Constants.USER_DIR);

    @Override
    public String getPropertiesLoad(String nameProp) throws IOException {
        String response = "";
        try (InputStream input = new FileInputStream("data.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            response = prop.getProperty(nameProp);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response;
    }

    @Override
    public ArrayList<String> getAllProperties() {
        return null;
    }
}
