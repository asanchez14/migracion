package DAL;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ModifyRequestParameterImpl implements IModifyRequestParameter {
    String path = System.getProperty(Constants.USER_DIR);
    private static FileWriter file;

    @Override
    public JSONObject getFileSuccessRequest(String nameOfFile, String absolutePath) throws FileNotFoundException, ParseException {
        FileReader reader = new FileReader(path + absolutePath + nameOfFile);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
        return jsonObject;
    }

    @Override
    public void setValueFromKeyIdentifier(String nameOfFile, String pathAbsolute, JSONObject jsonObject, String newFile) throws FileNotFoundException, ParseException {
        try {
            file = new FileWriter(path + pathAbsolute + newFile);
            file.write(jsonObject.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
