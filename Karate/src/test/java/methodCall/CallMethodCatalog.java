package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;
import utils.NamesGenerator;

import java.io.FileNotFoundException;

public class CallMethodCatalog {
    ModifyRequestParameterImpl modify;
    static String jsonCatalog = "requestBodyCatalogSuccessCreate.json";

    public CallMethodCatalog(){
        modify = new ModifyRequestParameterImpl();
    }

    public void setName(String item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("name", item);
        modify.setValueFromKeyIdentifier(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyCatalogNewFile.json");
    }

    public void setNameUpdate(String item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest("requestBodyUpdateCatalog.json", Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("name", item);
        modify.setValueFromKeyIdentifier(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyUpdateCatalog.json");
    }

    public String getNameRandom(){
        NamesGenerator namesGenerator = new NamesGenerator();
        return namesGenerator.nameRandom();
    }


    public void setDescription(String item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("description", item);
        modify.setValueFromKeyIdentifier(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyCatalogNewFile.json");
    }

    public void setBussinesLine(String item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("bussinessLineId", item);
        modify.setValueFromKeyIdentifier(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyCatalogNewFile.json");
    }

    public void setNumberCatalog(String item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest("numberOfCat.json", Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("number", item);
        modify.setValueFromKeyIdentifier(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "numberOfCat.json");
    }

    public String getNumberCatalog() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest("numberOfCat.json", Constants.PATH_PARTNER_REQUEST_JSON);
        return jsonObject.get("number").toString();
    }

    public void setNumberItemCatalog(String item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest("numberOfCat.json", Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("item", item);
        modify.setValueFromKeyIdentifier(jsonCatalog, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "numberOfCat.json");
    }

    public String getNumberItemCatalog() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest("numberOfCat.json", Constants.PATH_PARTNER_REQUEST_JSON);
        return jsonObject.get("item").toString();
    }

}
