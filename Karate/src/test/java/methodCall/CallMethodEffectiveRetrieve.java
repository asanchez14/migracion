package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;


public class CallMethodEffectiveRetrieve {

    ModifyRequestParameterImpl modify = new ModifyRequestParameterImpl();
    String nameOfFileSuccess = "requestBodyEfficacySuccess.json";
    String nameOfFileBadRequest = "requestBodyEfficacyBad.json";

    public JSONObject getAllObject() throws FileNotFoundException, ParseException {
        return modify.getFileSuccessRequest(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON);
    }

    public void setCertificationLarge() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        //JSONObject Certificado = (JSONObject) json.get("NumeroCertificado");
        json.put("NumeroCertificado", "@@#$%&/()");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// PRIMER NOMBRE ///
    public void setPrimerNombreLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonPrimerNombre","AAAAA@@@@@#$%&/()/&%$#@@@@");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonPrimerNombre",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// SEGUNDO NOMBRE ///
    public void setSegundoNombreLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonSegundoNombre","JOSÉ()=)(/&$%&/()=)(/&$%&/#");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonSegundoNombre",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO PATERNO ///
    public void setApellidoPaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonApellidoPaterno","Lópezzzzzzzzz@@@@#$%&/()#$%&");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonApellidoPaterno",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO MATERNO ///
    public void setApellidoMaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonApellidoMaterno","GUERREROOOOOOOOOOOOO@@@@$#%");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonApellidoMaterno",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// FECHA DE NACIMIENTO ///
    public void setFechaNacimientoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonFechaNacimiento","1986/11/030");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("Person");
        person.put("PersonFechaNacimiento",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_EFFICACY_RETRIEVE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

}
