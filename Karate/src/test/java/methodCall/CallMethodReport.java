package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;

public class CallMethodReport {

    ModifyRequestParameterImpl modify;
    static String json = "requestBodyReportsSuccess.json";
    static String jsonBad = "requestBodyFailed.json";

    public CallMethodReport(){
        modify = new ModifyRequestParameterImpl();
    }

    public void setPartnerId(int item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(json, Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("partnerId", item);
        modify.setValueFromKeyIdentifier(json, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, jsonBad);
    }

    public void setProductId(int item) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(json, Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("productId", item);
        modify.setValueFromKeyIdentifier(json, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, jsonBad);
    }

    public void setTypeValue() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(json, Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put("reportType", "TEST");
        modify.setValueFromKeyIdentifier(json, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, jsonBad);
    }

    public void setNulls(String key) throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(json, Constants.PATH_PARTNER_REQUEST_JSON);
        jsonObject.put(key, null);
        modify.setValueFromKeyIdentifier(json, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, jsonBad);
    }

}
