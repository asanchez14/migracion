package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;
import utils.NamesGenerator;

import java.io.FileNotFoundException;

public class CallMethodsIssuancePost {

    ModifyRequestParameterImpl modify = new ModifyRequestParameterImpl();
    String nameOfFileSuccess = "requestBodySuccess.json";
    String nameOfFileBadRequest = "requestBodyBad.json";

    public JSONObject getAllObject() throws FileNotFoundException, ParseException {
       return modify.getFileSuccessRequest(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON);
    }

    public void setProductIDNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("productId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setAssociatedIdNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("associateId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPlanIdNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("planId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderGivenNameNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("givenName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderSurNameNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("surname", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderOtherSurNameNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("otherSurname", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderBirthDateNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthDate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderBirthDateCountryIdNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthCountryId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderNationalityIdNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("nationalityId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderGenderIdNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("genderId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderPersonIdNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("personId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderAdressesAdressNameNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresses = (JSONArray) holder.get("addresses");
        JSONObject addressesObject = (JSONObject) adresses.get(0);
        addressesObject.put("addressName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderAdressesExternalNumberNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresses = (JSONArray) holder.get("addresses");
        JSONObject addressesObject = (JSONObject) adresses.get(0);
        addressesObject.put("externalNumber", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderAdressesPostalCodeNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresses = (JSONArray) holder.get("addresses");
        JSONObject addressesObject = (JSONObject) adresses.get(0);
        addressesObject.put("postalCode", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderGivenNameRandomName() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        String random = "PruebaBancoppel" + new NamesGenerator().nameRandom();
        holder.put("givenName", random);
        modify.setValueFromKeyIdentifier("requestBodyBancoppel.json", Constants.PATH_ISSUANSE_REQUEST_JSON,json, "requestBodyBancoppel.json");
    }

    public void setPolicySubjectInsuredSubjectDetailBirthLong() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject birthObj = (JSONObject) subjectDetail.get(0);
        birthObj.put("birthDate", "22/8809/2000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicySubjectInsuredSubjectDetailAgeMin() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject birthObj = (JSONObject) subjectDetail.get(0);
        birthObj.put("birthDate", "22/10/2003");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicySubjectInsuredSubjectDetailAgeMax() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject birthObj = (JSONObject) subjectDetail.get(0);
        birthObj.put("birthDate", "22/10/1900");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicySubjectInsuredSubjectDetailCatalogCountry() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject birthObj = (JSONObject) subjectDetail.get(0);
        birthObj.put("birthCountryId", "519856");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicySubjectInsuredSubjectDetailPersonType() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("personType", "PRUEBA");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicySubjectInsuredSubjectDetailPercent() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject beneficiary = (JSONObject) subjectDetail.get(0);
        JSONArray beneficiaryArr = (JSONArray) beneficiary.get("beneficiary");
        JSONObject obj = (JSONObject) beneficiaryArr.get(0);
        obj.put("percent", "50");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPaymentToken() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject payment = (JSONObject) json.get("payment");
        payment.put("token", "dRsdYWkTAs8+SCZ+5r0a9wEP6x+GiVGzdHuh/dDj8JTntf70+0AsaPR1gvnUVRFPcgrNAZDXddx7j9nHcW8hbl6xRScQPv5hE4RV45GF3gmeXeAPYH5wbzUfGdB/yOVDPDpWI//hmhHGk9AW1XuHE9kl3J/o/+88Yiwkvd99HDC7xkH+a9kFklZg6PwQ8OqZgTRiYZ28v2z2I6506W6vEnhAKbq/re2BWFHSr6BSl+IzUc52rldAPK+zg75aHSR6xllZ89lK+oOJ+8rfqKhm8hsoADFYlrvIddai+5egGuePPeOwYgZ1FI2f7m4BeHhUiGfOQCWNCsIZ3VHJC2ICSvnw6pmgooM6ZwxVS8CxxpQG+WcYkRVWcbAdnZ1E1uDLN8nBkKZf0eVnMLfTHoexGt7flbxVXylbC5pG+YFObTSFM2bSrLXZDuDONkM5r5wTCfy3+R53YlDiDVBEHKJYI5h2qriXZ77NDHDESbnnXZVxJlhHH5HsPmkBjxlLmmP/jX+u/NTPRSKQwOvZMZKBncHUa/QFW3CWze/hCHFhEHn+LFb4h0AEvKRHLdL/EblV8LgiWKue41rWj9sFWGqiC7SWBo1AqjdqBctZjjXOh5sKv6ZLKeozzb3OZ4K3wdHm915ADBP4vvvFrK5RnyOE6FZEuwZDV4MehgebavB9DcQ=");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCanalId() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject channel = (JSONObject) json.get("channel");
        channel.put("canalId", "546");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNotificationId() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject channel = (JSONObject) json.get("channel");
        channel.put("notificationId", "546");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setMedioId() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject channel = (JSONObject) json.get("channel");
        channel.put("medioId", "546");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderAdressesTownshipId() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresses = (JSONArray) holder.get("addresses");
        JSONObject addressesObject = (JSONObject) adresses.get(0);
        addressesObject.put("townshipId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderAdressesSuburbId() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresses = (JSONArray) holder.get("addresses");
        JSONObject addressesObject = (JSONObject) adresses.get(0);
        addressesObject.put("suburbId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderAdressesPhones() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray phones1 = (JSONArray) holder.get("phones");
        JSONObject phones = (JSONObject) phones1.get(0);
        phones.put("number", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicyHolderAdressesEmails() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray emails1 = (JSONArray) holder.get("emails");
        JSONObject email = (JSONObject) emails1.get(0);
        email.put("address", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPolicySubjectInsuredDesc() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject)
        policy.put("description", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPaymentTypeId() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject payment = (JSONObject) json.get("payment");
        payment.put("typeId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_ISSUANSE_REQUEST_JSON,json, nameOfFileBadRequest);
    }




}
