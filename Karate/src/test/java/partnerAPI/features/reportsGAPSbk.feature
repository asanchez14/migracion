Feature: Reportes GAP SBK

  Background:
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_reports')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def call = Java.type('methodCall.CallMethodReport')
    * def source = '/src/test/java/partnerAPI/features/request/'


  Scenario: Validar un partnerId Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    And def bodyRequest = read(userDir + source + 'requestBodyReportsSuccess.json')
    And request bodyRequest
    When method post
    Then status 200
    * print "Response ok: ", response
    And match response.data.fromDate == "#string", "#present"
    And match response.data == "#object", "#notnull"
    And match response.data.product == "#object", "#notnull"
    And match response.data.product.description == "#string", "#present"
    And match response.data.product.id == "#number", "#present"
    And match response.data.partner == "#object", "#notnull"
    And match response.data.partner.description == "#string", "#present"
    And match response.data.partner.id == "#number", "#present"
    And match response.data.downloadLink == "#present"
    And match response.data.assistant == "#object", "#notnull"
    And match response.data.assistant.description == "#string", "#present"
    And match response.data.assistant.id == "#number", "#present"
    And match response.data.toDate == "#string", "#present"
    And match response.data.id == "#number", "#present"
    And match response.data.type == "#string", "#present"
    And match response.data.status == "#string", "#present"
    And match response.notifications.[0].code contains 900
    And match response.notifications.[0].message contains "El reporte fue registrado correctamente"


  Scenario: Validar un partnerId no registrado Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setPartnerId('10000')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 500
    * print "Response ok: ", response
    And match response.trace contains "message=Socio no encontrado"

  Scenario: Validar reportType Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setTypeValue()
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications.[0].message contains "Error en el cuerpo de la peticion"

  Scenario: Validar reportType no nulo Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setNulls('reportType')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications.[0].message contains "Error en el cuerpo de la peticion"

    #
  Scenario: Validar partnerId no nulo Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setNulls('partnerId')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications.[0].message contains "Error en el cuerpo de la peticion"

  Scenario: Validar productId no nulo Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setNulls('productId')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 500

  Scenario: Validar assistantId no nulo Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setNulls('assistantId')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 500

  Scenario: Validar un productId no registrado Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setProductId('10000')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 500
    * print "Response ok: ", response
    And match response.trace contains "Producto no encontrado"

  Scenario: Validar al mandar nulo fromDate Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setNulls('fromDate')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 500


  Scenario: Validar al mandar nulo toDate Reportes principal
    * url getUrl + '/api/v1/reports'
    * header userId = 'PALOIT'
    * def setPartnerId = new call().setNulls('toDate')
    And def bodyRequest = read(userDir + source + 'requestBodyFailed.json')
    And request bodyRequest
    When method post
    Then status 500


