@ignore
Feature: Validación de API Partner policies/issuance HTTP POST

  Background:
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_issuance_subscribe')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/partnerAPI/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodsIssuancePost')

    ################################## Required params ########################################

  Scenario: Validar el parametro obligatorio porductId
    Given path '/api/v1/policies/issuance'
    * def javaMethod = new javaClass().setProductIDNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio associateId
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setAssociatedIdNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio planId
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPlanIdNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.givenName
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderGivenNameNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.surname
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderSurNameNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.otherSurname
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderOtherSurNameNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.birthDate
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderBirthDateNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.birthCountryId
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderBirthDateCountryIdNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.nationalityId
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderNationalityIdNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.personId
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderPersonIdNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.addresses.addressName
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderAdressesAdressNameNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.addresses.externalNumber
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderAdressesExternalNumberNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.addresses.postalCode
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderAdressesPostalCodeNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.addresses.townshipId
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderAdressesTownshipId()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"


  Scenario: Validar el parametro obligatorio policy.holder.addresses.suburbId
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderAdressesSuburbId()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.phones
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderAdressesPhones()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Validar el parametro obligatorio policy.holder.emails
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(5000)
    * def javaMethod = new javaClass().setPolicyHolderAdressesEmails()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

