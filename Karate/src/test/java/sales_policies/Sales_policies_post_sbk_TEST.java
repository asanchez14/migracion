package sales_policies.testClass;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import org.junit.Test;
import utils.Constants;
import utils.ReportGenerate;

import static org.junit.Assert.assertTrue;

@KarateOptions(features = Constants.SALES_POLICIES_SBK_POST_PATH)
public class Sales_policies_post_sbk_TEST {

    @Test
    public void testParallel() {
        ReportGenerate reportGenerate = new ReportGenerate();
        System.setProperty("karate.env", "demo");
        Results results = Runner.parallel(getClass(), 2);
        reportGenerate.generateReport(results.getReportDir(), Constants.PN_SALES_POLICIES_POST_SBK);
        assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
    }

}
