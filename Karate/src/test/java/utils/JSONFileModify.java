package utils;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class JSONFileModify {

    private static FileWriter file;
    String path = System.getProperty(Constants.USER_DIR);

    public JSONObject getDataFromFile(String nameOfFile) throws FileNotFoundException, ParseException {
        FileReader reader = new FileReader(path + Constants.PATH_SALES_REQUEST_JSON + nameOfFile);
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
        return jsonObject;
    }//getDataFromFile

    public void writeDataFile(String nameOfFile, String jsonData){
        try {
            file = new FileWriter(path + Constants.PATH_SALES_REQUEST_JSON + nameOfFile);
            file.write(jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }//writeDataFile

}
