package utils;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import java.io.FileNotFoundException;

public class SetNumber extends JSONFileModify {

    public void setNumberOfPolicies(String nameOfFile) throws ParseException, FileNotFoundException {
        JSONObject ret = getDataFromFile(nameOfFile);
        System.out.println(ret);
        JSONObject idObj = (
                (JSONObject) ret.get(Constants.POLICY_OBJECT)
        );
        GenerateNumberPolicy generateNumberPolicy = new GenerateNumberPolicy();
        String numberPolicy =  generateNumberPolicy.generateNumberOfPolicy();
        System.out.println("NUMBER GENERATED: " +numberPolicy);
        idObj.put(Constants.NUMBER, numberPolicy);
        writeDataFile(nameOfFile, ret.toJSONString());
    }

}
