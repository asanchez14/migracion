package simulation

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._
import scala.concurrent.duration._

class AllSimulations extends Simulation {


  val issuance = scenario(scenarioName = "subscribe (issuance)").exec(karateFeature(name = "classpath:partnerAPI/features/policies_issuance.feature"))
  val bancoppel = scenario(scenarioName = "policies bancoppel").exec(karateFeature(name = "classpath:Sales/features/sales_policies_post_bancoppel.feature"))
  val sbk = scenario(scenarioName = "policies sbk").exec(karateFeature(name = "classpath:Sales/features/sales_policies_post_sbk.feature"))
  val sbkVida = scenario(scenarioName = "policies sbk Vida").exec(karateFeature(name = "classpath:Sales/features/sales_policies_post_sbkvida.feature"))
  val vw = scenario(scenarioName = "policies vw").exec(karateFeature(name = "classpath:Sales/features/sales_policies_post_vw.feature"))

  setUp(
    issuance.inject(rampUsers(1) during (0 seconds)),
      bancoppel.inject(rampUsers(1) during (0 seconds)),
  sbk.inject(rampUsers(1) during (0 seconds)),
  sbkVida.inject(rampUsers(1) during (0 seconds)),
  vw.inject(rampUsers(1) during (0 seconds))
  )
}