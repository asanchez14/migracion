Feature: Validación de API Partner policies/issuance HTTP POST

  Background:
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_issuance_policies')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/partnerAPI/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodIssuanceBancoppel')

    ################################## SUCCESS RESPONSES ########################################

   ###### ALTA 1 ASEGURADO ######
  Scenario: Validar una respuesta valida del servicio issuance Bancoppel
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBodyIssuanceBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"

    ###### ALTA 2 ASEGURADOS ######
  Scenario: Validar respuesta valida del servicio issuance Bancoppel
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody2AseguradosBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"

     ###### ALTA 3 ASEGURADOS ######
  Scenario: Validar respuesta valida del servicio issuance Bancoppel
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody3AseguradosBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"

     ###### ALTA 4 ASEGURADOS ######
  Scenario: Validar respuesta valida del servicio issuance Bancoppel
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody4AseguradosBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"

    ###### ALTA 5 ASEGURADOS ######
  Scenario: Validar respuesta valida del servicio issuance Bancoppel
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody5AseguradosBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"

    ###### ALTA 6 ASEGURADOS ######
  Scenario: Validar respuesta valida del servicio issuance Bancoppel
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody6AseguradoBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"


    ################################ validations #######################################

   ###### NÚMERO CERTIFICADO ######

  Scenario: Número certificado en blanco responde code "008-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setNumeroCertificadoNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Número certificado con caracteres especiales y más de 30 caracteres responde code "023-PL,012-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setNumeroCertificadoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

      ###### NÚMERO SUCURSAL (CANAL) ######

  Scenario: Número sucursal en blanco responde code "010-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPolicySubjectInsuredSubjectDetailCatalogCountry()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Número sucursal con caracteres especiales, letras y más de 10 caracteres responde code "009-PL,015-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCanalLargoCaractaresEspecialesLetras()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### ID USUARIO ######

  Scenario: Id Usuario en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setIdUsuarioNull()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: ID usuario con caracteres especiales y más de 50 caracteres responde code "024-PL,035-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setIdUsuarioLargoCaractaresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

        ###### PRIMER NOMBRE ######
  Scenario: Campo primer nombre con caracteres especiales y más de 26 caracteres responde code "022-PL,011-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimerNombreLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo primer nombre con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimerNombreAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo primer nombre en blanco responde code "018-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimerNombreBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### SEGUNDO NOMBRE ######
  Scenario: Campo segundo nombre con caracteres especiales y más de 26 caracteres responde code "018-PL,023-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSegundoNombreLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo segundo nombre con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSegundoNombreAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo segundo nombre en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSegundoNombreBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

     ###### APELLIDO PATERNO ######
  Scenario: Campo apellido paterno con caracteres especiales y más de 26 caracteres responde code "002-PL,002-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoPaternoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo apellido paterno con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoPaternoAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo apellido paterno en blanco responde code "002-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoPaternoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

     ###### APELLIDO MATERNO ######
  Scenario: Campo apellido materno con caracteres especiales y más de 26 caracteres responde code "001-PL,001-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoMaternoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo apellido materno con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoMaternoAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo apellido materno en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoMaternoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

     ###### SEXO ######
  Scenario: Campo sexo con caracteres especiales, numero y más de 1 caracteres responde code "025-PL,036-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSexoLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo sexo con acento letra F
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSexoF()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo sexo con acento letra M
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSexoM()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo sexo en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSexoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

    ###### FECHA DE NACIMIENTO ######
  Scenario: Campo fecha de nacimiento con formato incorrecto (YYYY/MM/DD) y más de 10 caracteres responde code "019-PL,026-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaNacimientoLargaFormatoIncorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo fecha de nacimiento mayor a la actual
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaNacimientoMayorActual()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo fecha nacimiento asegurado contratatnte mayor de 75 años responde code "014-PN"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaNacimientoMayorDeLoPermitido()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo fecha nacimiento asegurado contratatnte menor de 18 años responde code "015-PN"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaNacimientoMenorDeEdad()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo fecha nacimiento en blanco responde code "012-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaNacimientoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

     ###### ESTADO DE NACIMIENTO ######
  Scenario: Campo estado de nacimiento con caracteres especiales y más de 2 caracteres responde code "035-PL,028-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEstadoNacimientoLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo estado de nacimiento no esta en el catálogo responde code "030-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEstadoNacimientoNoRegistrado()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo estado nacimiento en blanco responde code "024-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEstadoNacimientoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### CALLE ######
  Scenario: Campo calle con caracteres especiales y más de 40 caracteres responde code "032-PT,009-PTT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCalleLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo calle no esta en el catálogo responde code "057-PN"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCalleNoRegistrado()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo calle en blanco responde code "006-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCalleBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

     ###### NO. ESTERIOR ######
  Scenario: Campo No.Exterior con caracteres especiales y más de 15 caracteres responde code "010-PL,016-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setNoExteriorLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo No.Exterior en blanco responde code "011-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setNoExteriorBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

     ###### NO. INTERIOR ######
  Scenario: Campo No.Interior con caracteres especiales y más de 15 caracteres responde code "011-PL,018-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setNoInteriorLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo No.Interior en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setNoInteriorBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

     ###### CÓDIGO POSTAL ######
  Scenario: Campo CP con caracteres especiales y más de 5 caracteres responde code "004-PL,005-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCPLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo CP no esta en el catálogo responde code "006-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCPNoRegistrado()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo CP en blanco responde code "004-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCPBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

      ###### COLONIA ######
  Scenario: Campo colonia con caracteres especiales y más de 4 caracteres responde code "005-PL,007-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setColoniaLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo colonia no esta en el catálogo responde code "008-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setColoniaNoRegistrado()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo colonia en blanco responde code "005-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setColoniaBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### DELEGACIÓN / MUNICIPIO ######
  Scenario: Campo delegación / municipio con caracteres especiales, letras y más de 4 caracteres responde code "021-PL,025-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setMunicipioLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo delegación / municipio en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setMunicipioBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

    ###### CIUDAD / ESTADO ######
  Scenario: Campo ciudad / estado con caracteres especiales y más de 50 caracteres responde code "003-PL,003-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCiudadLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo ciudad / estado no esta en el catálogo responde code "004-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCiudadNoRegistrado()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo ciudad / estado en blanco responde code "003-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCiudadBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

     ###### RFC ######
  Scenario: Campo RFC con caracteres especiales y más de 20 caracteres responde code "017-PL,024-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setRFCLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo RFC con letra Ñ
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setRFCConLetraN()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo RFC en blanco responde code "023-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setRFCBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### TELÉFONO ######
  Scenario: Campo teléfono con caracteres especiales y letras responde code "022-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setTelefonoLetrasCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo teléfono con más de 30 digitos responde code "015-PL"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setTelefonoLargo()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo teléfono con menor de 8 digitos responde code "015-PL"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setTelefonoCorto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo teléfono en blanco responde code "029-PO,029-PO,015-PL"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEmailBlanco()
    * def javaMethod = new javaClass().setTelefonoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### EMAIL ######
  Scenario: Campo email con caracteres especiales responde code "027-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEmailCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo email con formato incorrecto sin @ responde code "027-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEmailSinArroba()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo email con formato incorrecto sin punto antes del dominio responde code "027-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEmailSinDominio()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo email en blanco responde code "029-PO,029-PO,015-PL"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setTelefonoBlanco()
    * def javaMethod = new javaClass().setEmailBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo email con caracteres especiales permitidos ("_" y "-")
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEmailCaracteresEspecialesPermitidos()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

    ########## DATOS MIGRANTE ##########

    ###### PRIMER NOMBRE ######
  Scenario: Campo primer nombre migrante con caracteres especiales y más de 26 caracteres responde code "022-PL,011-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimerNombreMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo primer nombre con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimerNombreMigranteAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo primer nombre en blanco responde conde "018-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimerNombreMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### SEGUNDO NOMBRE ######
  Scenario: Campo segundo nombre con caracteres especiales y más de 26 caracteres responde code "018-PL,023-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSegundoNombreMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo segundo nombre con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSegundoNombreMigranteAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo segundo nombre en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setSegundoNombreMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

     ###### APELLIDO PATERNO ######
  Scenario: Campo apellido paterno con caracteres especiales y más de 26 caracteres responde code "002-PL,002-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoPaternoMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo apellido paterno con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoPaternoMigranteAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo apellido paterno en blanco responde code "002-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoPaternoMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

     ###### APELLIDO MATERNO ######
  Scenario: Campo apellido materno con caracteres especiales y más de 26 caracteres responde code "001-PL,001-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoMaternoMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo apellido materno con acento, letra Ñ y compuesto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoMaternoMigranteAcentoCompuesto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo apellido materno en blanco
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setApellidoMaternoMigranteBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

     ###### FECHA INICIO SEGURO ######
  Scenario: Campo fecha inicio seguro fecha actual
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().getDate()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo fecha inicio seguro en blanco responde code "017-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaInicioSeguroBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo fecha inicio seguro formato incorrecto (YYYY/MM/DD) y con más de 10 caracteres responde code "044-PN,020-PL,010-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaInicioSeguroFormatoIncorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo fecha inicio seguro mayor a el día actual responde code "043-PN"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaInicioSeguroMayorDiaActual()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo fecha inicio seguro menor a el día actual responde code "044-PN"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setFechaInicioSeguroMenorDiaActual()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

      ###### PERIODO DE PAGO ######
  Scenario: Campo periodo de pago correcto
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPeriodoDePagoCorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo perido de pago con caracteres especiales, letra y más de 2 caracteres responde code "013-PL,020-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPeriodoPagoCaracteresEspecialesLargo()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo periodo de pago en blanco responde code "014-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPeriodoPagoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

     ###### PRIMA PÓLIZA ######
  Scenario: Campo prima póliza con caracteres especiales, letras y más de 5 caracteres responde code "012-PL,041-PT"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimaPolizaLargaCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo prima póliza correcta"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimaPolizaCorrecto()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#array"

  Scenario: Campo prima póliza en blanco responde code "030-PO"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimaPolizaBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo prima póliza no corresponde al periodo de pago 4 responde code "055-PN"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimaPolizaNoCoincideConPeriodoPago4()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Campo prima póliza no corresponde al periodo de pago 5 responde code "055-PN"
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPrimaPolizaNoCoincideConPeriodoPago5()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### ALTA 7 ASEGURADOS ######
  Scenario: Validar que no se pueda dar de alta un septimo asegurado en una póliza con 6 asegurados responde code "008-PN"
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody7AseguradosBancoppel.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#object"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

    ###### ALTA ASEGURADO 7 ######
  Scenario: Validar que se pueda dar de alta un septimo asegurado en una póliza que cancelo 1 de los 6 espacios ocupados
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody7AseguradosBancoppe1.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"

    ###### ALTA DE 10 ASEGURADO ######
  Scenario: Validar que se pueda dar de alta un 10 asegurados en una póliza que cancelo 4 de los 6 espacios ocupados
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBody7AseguradosBancoppe1.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"

     ###### ALTA DE 1 ASEGURADO CON SOLO EL CAMPO TELÉFONO  ######
  Scenario: Validar que se agregue el asegurado con teléfono y email
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setEmailBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response


   ###### ALTA DE 1 ASEGURADO CON SOLO EL CAMPO EMAIL  ######
  Scenario: Validar que se agregue el asegurado con teléfono y email
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setTelefonoBlanco()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response

     ###### ALTA DE 1 ASEGURADO CON PÓLIZA DE 6 MESES  ######
  Scenario: Validar que se genere la póliza por 6 meses
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPeriodoDePago6Meses()
    * def javaMethod = new javaClass().setPrimaPolizaNoCoincideConPeriodoPago6()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response

    ###### ALTA DE 1 ASEGURADO CON PÓLIZA DE 12 MESES  ######
  Scenario: Validar que se genere la póliza por 12 meses
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPeriodoDePago12Meses()
    * def javaMethod = new javaClass().setPrimaPolizaNoCoincideConPeriodoPago12()
    And def bodyRequest = read(userDir + source + 'requestBodyBadBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response