Feature: Update

  Background:

    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_update_policies')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/Bancoppel/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodUpdate')
    * def RequestBody = read(userDir + source + 'requestBodyUpdate.json')
 #   * def RequestBody1 = read(userDir + source + 'requestBodyUpdateMigrante.json')

#  Scenario: Datos correctos cancelación
#    Given path 'v1/update_policy'
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.NoSucursal == "99"
#    And match response.updateMultiPolicy.IdUsuario == "UO0020"
#    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "José"
#    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == "de Jesús"
#    And match response.updateMultiPolicy.person.[0].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[0].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[0].PersonFechaNacimiento == "17/06/1992"
#    And match response.updateMultiPolicy.person.[0].PersonEstadoNacimiento == "11"
#    And match response.updateMultiPolicy.person.[0].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[0].PersonEmail == "fmarquez.ing@gmail.com"
#    And match response.updateMultiPolicy.person.[0].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817"
#    And match response.updateMultiPolicy.person.[0].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817"
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"


    #### NO. SUCURSAL #####
#  Scenario: No. Sucursal con caracteres especiales, letras y más de 10 caracteres responde code 015-PT,009-PL
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNoSubsidiaryLargoCharactersEspecial()
#    And def RequestBody = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "015-PT,009-PL"

  Scenario: No. Sucursal en blanco responde code 010-PO
    Given path 'v1/update_policy'
    * def javaMethod = new javaClass().setNoSubsidiaryBlanco()
    And def RequestBody = read(userDir + source + 'requestBodyBadUpdate.json')
    And request RequestBody
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.updateMultiPolicy.CodResp contains "011-PO"
#
#      ##### ID USUARIO #####
#  Scenario: Id usuario con caracteres especiales y más de 50 caracteres responde code 035-PT,024-PL
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setIdUserLargoCharactersEspecialLetters()
#    And def RequestBody = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "035-PT,024-PL"
#
#  Scenario: Id usuario en blanco responde code 021-PO
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setIdUserBlanco()
#    And def RequestBody = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "021-PO"
#
#    ##### PRIMER NOMBRE ASEGURADO #####
#  Scenario: Primer nombre del asegurado con caracteres especiales y más de 26 caracteres responde code 022-PL,011-PT
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setFirstNameLargoCharacterEspecial()
#    And def RequestBody = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "022-PL,011-PT"
#
#  Scenario: Primer nombre del asegurado en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setPrimerNameBlanco()
#    And def RequestBody = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == ""

#  Scenario: Primer nombre del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setPrimerNameAcentoCompuesto()
#    And def RequestBody = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonPrimerNombre == "José Toño"


    #### SEGUNDO NOMBRE ASEGURADO #####
#  Scenario: Segundo nombre del asegurado con caracteres especiales y más de 26 caracteres responde code "018-PL y 023-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSegundoNombreLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "018-PL,023-PT"

#  Scenario: Segundo nombre del asegurado en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSegundoNombreBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == ""

#  Scenario: Segundo nombre del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSegundoNombreAcentoCompuesto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == "Toño de Jesús"


    #### APELLIDO PATERNO ASEGURADO #####
#  Scenario: Apellido paterno del asegurado con caracteres especiales y más de 26 caracteres responde code "002-PL y 002-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setApellidoPaternoLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "002-PL,002-PT"

#  Scenario : Apellido paterno del asegurado en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    And request RequestBody[11]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonApellidoPaterno == ""

#  Scenario : Apellido materno del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    And request RequestBody[12]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonApellidoPaterno == "Riaño-López"


     #### APELLIDO MATERNO ASEGURADO #####
#  Scenario: Apellido materno del asegurado con caracteres especiales y más de 26 caracteres responde code "001-PL y 001-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setApellidoMaternoLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "001-PL,001-PT"

#  Scenario : Apellido materno del asegurado en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setApellidoMaternoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == ""

#  Scenario : Apellido materno del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setApellidoMaternoAcentoCompuesto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == "Riaño-López"


      #### SEXO ASEGURADO #####
#  Scenario: Sexo del asegurado con caracteres especiales, más de 1 caracter, numerico y diferente a "M" o "F" responde code "025-PL y 036-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSexoLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "025-PL,036-PT"
#
#  Scenario: Sexo del asegurado en blanco responde code "026-PO"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSexoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "026-PO"

#  Scenario : Sexo del asegurado con letra "M"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSexoM()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSexo == "M"
#
#  Scenario : Sexo del asegurado con letra "F"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSexoF()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSexo == "F"


       #### FECHA DE NACIMIENTO ASEGURADO #####
#  Scenario: Fecha nacimiento del asegurado con formato incorrecto empezando por año y más de 10 caracteres responde code "054-PN y 026-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setFechaNacimientoLargaFormatoIncorrecto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "054-PN,026-PT"

#  Scenario : Fecha nacimiento del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setFechaNacimientoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonFechaNacimiento == ""

#  Scenario : Fecha de nacimineot del asegurado con fecha mayor a la actual responde code "054-PN"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setFechaNacimientoMayorActual()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "054-PN"
#
#      #### CONTRATANTE ASEGURADO MAYOR DE 75 AÑOS #####
#  Scenario: Actualizar la fecha de nacimiento del asegurado mayor a 75 años responde code "014-PN"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setFechaNacimientoMayorDeLoPermitido()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "014-PN"
#
#
#    #### CONTRATANTE ASEGURADO MENOR DE 18 AÑOS #####
#  Scenario: Actualizar la fecha de nacimiento del asegurado menor a 18 años responde code "015-PN"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setFechaNacimientoMenorDeEdad()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "015-PN"


     #### ESTADO DE NACIMIENTO ASEGURADO #####
#  Scenario: Estado nacimiento del asegurado con caracteres especiales y más de 2 caracteres responde code "035-PL y 028-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEstadoNacimientoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "035-PL,028-PT"

#  Scenario : Estado de nacimiento del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEstadoNacimientoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonEstadoNacimiento == ""

#  Scenario: Estado de nacimiento del asegurado no esta registrado en el catálogo responde code "030-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEstadoNacimientoNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "030-PT"
#
#    #### NACIONALIDAD ASEGURADO #####
#  Scenario: Nacionalidad del asegurado con caracteres especiales y más de 3 caracteres responde code "026-PL y 040-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNacionalidadLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "026-PL,040-PT"
#
#  Scenario: Estado de nacimiento del asegurado en blanco responde conde "027-PO"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNacionalidadBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "027-PO"
#
#  Scenario: Nacionalidad del asegurado no esta registrado en el catálogo responde code "013-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNacionalidadNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "013-PT"
#
#     #### CALLE ASEGURADO #####
#  Scenario: Calle del asegurado con caracteres especiales y más de 40 caracteres responde code "029-PL y 009-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCalleLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "029-PL,009-PT"

#  Scenario : Calle del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCalleBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCalle == ""

#  Scenario: Calle del asegurado no esta registrado en el catálogo responde code "057-PN"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCalleNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "057-PN"
#
#      #### NO.EXTERIOR ASEGURADO #####
#  Scenario: No.exterior del asegurado con caracteres especiales y más de 15 caracteres responde code "010-PL y 016-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNoExteriorLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "010-PL,016-PT"
#
##  Scenario : No.exterior del asegurado en blanco no es un campo obligatorio
##    Given path 'v1/update_policy'
##    * def javaMethod = new javaClass().setNoExteriorBlanco()
##    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
##    And request RequestBody
##    When method put
##    Then status 200
##    And print 'Response is: ', response
##    And match response.updateMultiPolicy.person.[0].PersonNumeroExterior == ""
#
#    #### NO.INTERIOR ASEGURADO #####
#  Scenario: No.interior del asegurado con caracteres especiales y más de 15 caracteres responde code "011-PL y 018-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNoInteriorLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "011-PL,018-PT"

#  Scenario : No.interior del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNoInteriorBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonNumeroInterior == ""

     #### CÓDIGO POSTAL #####
#  Scenario: CP del asegurado con caracteres especiales y más de 40 caracteres responde code "029-PL y 009-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCPLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "029-PL,009-PT"

#  Scenario : CP del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCPBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCP == ""
#
#  Scenario: CP del asegurado no esta registrado en el catálogo responde code "006-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCPNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "006-PT"
#
#    #### COLONIA #####
#  Scenario: Colonia del asegurado con caracteres especiales y más de 4 caracteres responde code "005-PL,007-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setColoniaLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "029-PL,009-PT"
#
#  Scenario : Colonia del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setColoniaBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCP == ""
#
#  Scenario: Colonia del asegurado no esta registrado en el catálogo responde code "008-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setColoniaNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "008-PT"
#
#      #### DELEGACIÓN / MUNICIPIO #####
#  Scenario: Delegación / Municipio del asegurado con caracteres especiales y más de 4 caracteres responde code "021-PL,025-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setMunicipioLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "021-PL,025-PT"
#
#  Scenario : Delegación / Municipio del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setMunicipioBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonDelegacionMunicipio == ""
#
#     #### CIUDAD / ESTADO #####
#  Scenario: Ciudad / Estado del asegurado con caracteres especiales y más de 50 caracteres responde code "003-PL,003-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCiudadLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "003-PL,003-PT"
#
#  Scenario : Ciudad / Estado del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCiudadBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCP == ""
#
#  Scenario: Ciudad / Estado del asegurado no esta registrado en el catálogo responde code "004-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCiudadNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "004-PT"
#
#    #### TELÉFONO #####
#  Scenario: Teléfono del asegurado con caracteres especiales y letras responde code "003-PL,003-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setTelefonoLetrasCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "003-PL,003-PT"
#
#  Scenario: Teléfono del asegurado más de 30 digitos responde code "015-PL"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setTelefonoLargo()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "015-PL"
#
#  Scenario: Teléfono del asegurado menos de 8 digitos responde code "015-PL"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setTelefonoCorto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "015-PL"
#
#  Scenario : Teléfono del asegurado en blanco
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setTelefonoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#
#       ##### EMAIL ASEGURADO #####
#  Scenario : Email del asegurado con caracteres especiales permitidos ("-" y "_")
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEmailBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonEmail == "art._61-2@hotmail.com"
#
#  Scenario : Email del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEmailBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonEmail == ""
#
#      #### NUMERO DE CERTIFICADO ASEGURADO #####
#  Scenario: Numero de certificado del asegurado con caracteres especiales y más de 30 caracteres responde code "023-PL,012-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNumeroCertificadoLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "023-PL,012-PT"
#
#  Scenario: Numero de certificado del asegurado en blanco es un campo obligatorio responde code "008-PO"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNumeroCertificadoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "008-PO"
#
#     #### NUMERO DE PÓLIZA ASEGURADO #####
#  Scenario: Numero de póliza del asegurado con caracteres especiales y más de 35 caracteres responde code "033-PL y 019-PT"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNumeroPolizaLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "033-PL,019-PT"
#
#  Scenario : Numero de póliza del asegurado en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNumeroPolizaBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PolizaNumero == ""
#
#    ########### DATOS MIGRANTE ############
#
#      #### PRIMER NOMBRE ASEGURADO MIGRANTE #####
  Scenario: Primer nombre del asegurado migrante con caracteres especiales y más de 26 caracteres responde code "022-PL" y "011-PT"
    Given path 'v1/update_policy/1'
    * def javaMethod = new javaClass().setPrimerNombreMigranteLargoCaracteresEspeciales()
    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
    And request bodyRequest
    When method put
    Then status 400
    And print 'Response is: ', response
    And match response.updateMultiPolicy.CodResp contains "022-PL,011-PT"

#  Scenario : Primer nombre del asegurado migrante en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    And request RequestBody1[1]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == ""
#
#  Scenario : Primer nombre del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    And request RequestBody1[2]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "José Toño"
#
#
#     #### SEGUNDO NOMBRE ASEGURADO MIGRANTE #####
#  Scenario: Segundo nombre del asegurado migrante con caracteres especiales y más de 26 caracteres responde code "018-PL y 023-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setSegundoNombreMigranteLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "018-PL,023-PT"
#
#  Scenario : Segundo nombre del asegurado en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSegundoNombreMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == ""
#
#  Scenario : Segundo nombre del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setSegundoNombreMigranteAcentoCompuesto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSegundoNombre == "Toño de Jesús"
#
#
#    #### APELLIDO PATERNO ASEGURADO MIRANTE#####
#  Scenario: Apellido paterno del asegurado migrante con caracteres especiales y más de 26 caracteres responde code "002-PL y 002-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setApellidoPaternoMigranteLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "002-PL,002-PT"
#
#  Scenario : Apellido paterno del asegurado en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    And request RequestBody1[7]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == ""
#
#  Scenario : Apellido paterno del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    And request RequestBody1[8]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Riaño-López"
#
#
#     #### APELLIDO MATERNO ASEGURADO #####
#  Scenario: Apellido materno del asegurado con caracteres especiales y más de 26 caracteres responde code "001-PL y 001-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setApellidoMaternoMigranteLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "001-PL,001-PT"
#
#  Scenario : Apellido materno del asegurado en blanco es un campo opcional
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setApellidoMaternoMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == ""
#
#  Scenario : Apellido materno del asegurado con acento y letra ñ
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setApellidoMaternoMigranteAcentoCompuesto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonApellidoMaterno == "Riaño-López"
#
#
#      #### SEXO ASEGURADO MIGRANTE #####
#  Scenario: Sexo del asegurado migrante con caracteres especiales, más de 1 caracter, numerico y diferente a "M" o "F" responde code "025-PL y 036-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setSexoMigranteLargoCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "025-PL,036-PT"
#
#  Scenario: Sexo del asegurado migrante en blanco responde code "026-PO"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setMigranteSexoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "026-PO"
#
#  Scenario : Sexo del asegurado migrante con letra "M"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setMigranteSexoM()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSexo == "M"
#
#  Scenario : Sexo del asegurado migrante con letra "F"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setMigranteSexoF()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonSexo == "F"
#
#
#       #### FECHA DE NACIMIENTO ASEGURADO MIGRANTE #####
#  Scenario: Fecha nacimiento del asegurado migrante con formato incorrecto empezando por año y más de 10 caracteres responde code "054-PN y 026-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setFechaNacimientoMigranteLargaFormatoIncorrecto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "054-PN,026-PT"
#
#  Scenario : Fecha nacimiento del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setFechaNacimientoMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonFechaNacimiento == ""
#
#  Scenario: Fecha de nacimineot del asegurado migrante con fecha mayor a la actual responde code "054-PN"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setFechaNacimientoMigranteMayorActual()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "054-PN"
#
#      #### ASEGURADO MIGRANTE MAYOR DE 75 AÑOS #####
#  Scenario: Actualizar la fecha de nacimiento del asegurado migrante mayor a 75 años responde code "014-PN"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setFechaNacimientoMayorMigranteDeLoPermitido()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "014-PN"
#
#
#    #### ASEGURADO MIGRANTE MENOR DE 18 AÑOS #####
#  Scenario: Actualizar la fecha de nacimiento del asegurado migrante menor a 18 años responde code "015-PN"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setFechaNacimientoMigranteMenorDeEdad()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "015-PN"
#
#      #### ESTADO DE NACIMIENTO ASEGURADO MIGRANTE #####
#  Scenario: Estado nacimiento del asegurado migrante con caracteres especiales y más de 2 caracteres responde code "035-PL y 028-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setEstadoNacimientoMigranteCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "035-PL,028-PT"
#
#  Scenario : Estado de nacimiento del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEstadoNacimientoMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonEstadoNacimiento == ""
#
#  Scenario: Estado de nacimiento del asegurado migrante no esta registrado en el catálogo responde code "030-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setEstadoNacimientoMigranteNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "030-PT"
#
#       #### NACIONALIDAD ASEGURADO MIGRANTE #####
#  Scenario: Nacionalidad del asegurado migrante con caracteres especiales y más de 3 caracteres responde code "026-PL y 040-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNacionalidadMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "026-PL,040-PT"
#
#  Scenario: Estado de nacimiento del asegurado migrante en blanco responde conde "027-PO"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNacionalidadMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "027-PO"
#
#  Scenario: Nacionalidad del asegurado migrante no esta registrado en el catálogo responde code "013-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNacionalidadMigranteNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "013-PT"
#
#      #### CALLE ASEGURADO MIGRANTE #####
#  Scenario: Calle del asegurado migrante con caracteres especiales y más de 40 caracteres responde code "029-PL y 009-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCalleMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "029-PL,009-PT"
#
#  Scenario : Calle del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCalleMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCalle == ""
#
#  Scenario: Calle del asegurado migrante no esta registrado en el catálogo responde code "057-PN"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCalleMigranteNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "057-PN"
#
#     #### NO.EXTERIOR ASEGURADO MIGRANTE #####
#  Scenario: No.exterior del asegurado migrante con caracteres especiales y más de 15 caracteres responde code "010-PL y 016-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNoExteriorMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "010-PL,016-PT"
#
#  Scenario : No.exterior del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNoExteriorMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonNumeroExterior == ""
#
#    #### NO.INTERIOR ASEGURADO MIGRANTE #####
#  Scenario: No.interior del asegurado migrante con caracteres especiales y más de 15 caracteres responde code "011-PL y 018-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNoInteriorMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "011-PL,018-PT"
#
#  Scenario : No.interior del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNoInteriorMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonNumeroInterior == ""
#
#     #### CÓDIGO POSTAL MIGRANTE #####
#  Scenario: CP del asegurado migrante con caracteres especiales y más de 40 caracteres responde code "029-PL y 009-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCPMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "029-PL,009-PT"
#
#  Scenario : CP del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCPMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCP == ""
#
#  Scenario: CP del asegurado migrante no esta registrado en el catálogo responde code "006-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCPMigranteNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "006-PT"
#
#    #### COLONIA #####
#  Scenario: Colonia del asegurado migrante con caracteres especiales y más de 4 caracteres responde code "005-PL,007-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setColoniaMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "029-PL,009-PT"
#
#  Scenario : Colonia del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setColoniaMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCP == ""
#
#  Scenario: Colonia del asegurado migrante no esta registrado en el catálogo responde code "008-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setColoniaMigranteNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "008-PT"
#
#     #### DELEGACIÓN / MUNICIPIO MIGRANTE #####
#  Scenario: Delegación / Municipio del asegurado migrante con caracteres especiales y más de 4 caracteres responde code "021-PL,025-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setMunicipioMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "021-PL,025-PT"
#
#  Scenario : Delegación / Municipio del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setMunicipioMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonDelegacionMunicipio == ""
#
#      #### CIUDAD / ESTADO MIGRANTE #####
#  Scenario: Ciudad / Estado del asegurado migrante con caracteres especiales y más de 50 caracteres responde code "003-PL,003-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCiudadMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "003-PL,003-PT"
#
#  Scenario : Ciudad / Estado del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCiudadMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonCP == ""
#
#  Scenario: Ciudad / Estado del asegurado migrante no esta registrado en el catálogo responde code "004-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCiudadMigranteNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "004-PT"
#
#    #### TELÉFONO MIGRANTE #####
#  Scenario: Teléfono del asegurado migrante con caracteres especiales y letras responde code "003-PL,003-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setTelefonoMigranteLetrasCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "003-PL,003-PT"
#
#  Scenario: Teléfono del asegurado migrante más de 30 digitos responde code "015-PL"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setTelefonoMigranteLargo()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "015-PL"
#
#  Scenario: Teléfono del asegurado migrante menos de 8 digitos responde code "015-PL"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setTelefonoMigranteCorto()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "015-PL"
#
#  Scenario : Teléfono del asegurado migrante en blanco
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setTelefonoMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#
#     ##### EMAIL ASEGURADO MIGRANTE #####
#  Scenario : Email del asegurado migrante con caracteres especiales permitidos ("-" y "_")
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEmailMigranteCaracteresEspecialesPermitidos()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonEmail == "art_6-12@hotmail.com"
#
#  Scenario : Email del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setEmailMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[0].PersonEmail == ""
#
#      #### RFC ASEGURADO MIGRANTE #####
#  Scenario: RFC del asegurado migrante con caracteres especiales y más de 20 caracteres responde code "017-PL y 024-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setRFCLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "017-PL,024-PT"
#
#  Scenario : RFC del asegurado migrante en blanco no es obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setRFCBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == ""
#
#  Scenario : RFC del asegurado migrante con letra "Ñ"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setRFCConLetraN()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#
#    #### CURP ASEGURADO MIGRANTE #####
#  Scenario: CURP del asegurado migrante con caracteres especiales y más de 18 caracteres responde code "006-PL y 037-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCURPLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "006-PL,037-PT"
#
#  Scenario : CURP del asegurado migrante en blanco no es obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setCURPBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonCurp == ""
#
#  Scenario: CURP del asegurado migrante con letra "Ñ" responde code "037-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setCURPConLetraN()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "037-PT"
#
#
#      #### PARENTESCO ASEGURADO MIGRANTE #####
#  Scenario: Parentesco del asegurado migrabte con más de 2 caracteres responde code "028-PL"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setParentescoLarga()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "028-PL"
#
#  Scenario: Parentesco del asegurado migrante en blanco es un campo obligatorio responde code "BWENGINE-100030"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setParentescoBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "BWENGINE-100030"
#
#  Scenario: Parentesco del asegurado migrante no se encuentra en el catálogo responde code "017-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setParentescoNoRegistrado()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "017-PT"
#
#
#      #### NUMERO DE CERTIFICADO ASEGURADO MIGRANTE #####
#  Scenario: Numero de certificado del asegurado migrante con caracteres especiales y más de 32 caracteres responde code "037-PL,012-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNumeroCertificadoMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "037-PL,012-PT"
#
#  Scenario: Numero de certificado del asegurado migrante en blanco es un campo obligatorio responde code "008-PO"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNumeroCertificadoMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "008-PO"
#
#     ##### NUMERO DE PÓLIZA ASEGURADO MIGRANTE #####
#  Scenario: Numero de póliza del asegurado migrante con caracteres especiales y más de 35 caracteres responde code "033-PL y 019-PT"
#    Given path 'v1/update_policy/1'
#    * def javaMethod = new javaClass().setNumeroPolizaMigranteLargaCaracteresEspeciales()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request bodyRequest
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.CodResp contains "033-PL,019-PT"

#  Scenario : Numero de póliza del asegurado migrante en blanco no es un campo obligatorio
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNumeroPolizaMigranteBlanco()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PolizaNumero == ""
#
#
#     ##### ACTUALIZACIÓN DE 1 ASEGURADO MIGRANTE #####
#  Scenario : Actualización de 1 asegurado migrante correcto
#    Given path 'v1/update_policy'
#    And request RequestBody1[0]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#
#
#   ##### ACTUALIZACIÓN DE 2 ASEGURADOS MIGRANTES #####
#  Scenario : Actualización de 2 asegurados migrantes correcto
#    Given path 'v1/update_policy'
#    And request RequestBody1[1]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#    ########## SEGUNDO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[2].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[2].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[2].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[2].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[2].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[2].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[2].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[2].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[2].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-2"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-2"
#
#
#    ##### ACTUALIZACIÓN DE 3 ASEGURADOS MIGRANTES #####
#  Scenario : Actualización de 3 asegurados migrantes correcto
#    Given path 'v1/update_policy'
#    And request RequestBody1[2]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#    ########## SEGUNDO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[2].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[2].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[2].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[2].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[2].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[2].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[2].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[2].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[2].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-2"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-2"
#    ########## TERCER ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[3].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[3].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[3].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[3].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[3].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[3].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[3].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[3].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[3].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-3"
#
#
#    ##### ACTUALIZACIÓN DE 4 ASEGURADOS MIGRANTES #####
#  Scenario : Actualización de 4 asegurados migrantes correcto
#    Given path 'v1/update_policy'
#    And request RequestBody1[3]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#    ########## SEGUNDO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[2].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[2].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[2].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[2].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[2].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[2].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[2].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[2].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[2].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-2"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-2"
#    ########## TERCER ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[3].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[3].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[3].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[3].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[3].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[3].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[3].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[3].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[3].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-3"
#    ########## CUARTO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[4].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[4].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[4].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[4].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[4].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[4].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[4].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[4].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[4].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[4].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[4].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-4"
#    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-4"
#
#
#        ##### ACTUALIZACIÓN DE 5 ASEGURADOS MIGRANTES #####
#  Scenario : Actualización de 5 asegurados migrantes correcto
#    Given path 'v1/update_policy'
#    And request RequestBody1[4]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#    ########## SEGUNDO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[2].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[2].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[2].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[2].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[2].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[2].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[2].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[2].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[2].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-2"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-2"
#    ########## TERCER ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[3].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[3].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[3].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[3].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[3].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[3].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[3].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[3].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[3].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-3"
#    ########## CUARTO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[4].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[4].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[4].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[4].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[4].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[4].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[4].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[4].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[4].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[4].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[4].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-4"
#    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-4"
#    ########## QUINTO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[5].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[5].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[5].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[5].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[5].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[5].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[5].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[5].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[5].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[5].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[5].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[5].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-5"
#    And match response.updateMultiPolicy.person.[5].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-5"
#
#
#    ##### ACTUALIZACIÓN DE 6 ASEGURADOS MIGRANTES #####
#  Scenario : Actualización de 6 asegurados migrantes correcto
#    Given path 'v1/update_policy'
#    And request RequestBody1[5]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#    ########## SEGUNDO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[2].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[2].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[2].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[2].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[2].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[2].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[2].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[2].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[2].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-2"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-2"
#    ########## TERCER ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[3].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[3].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[3].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[3].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[3].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[3].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[3].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[3].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[3].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[3].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-3"
#    And match response.updateMultiPolicy.person.[3].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-3"
#    ########## CUARTO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[4].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[4].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[4].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[4].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[4].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[4].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[4].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[4].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[4].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[4].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[4].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-4"
#    And match response.updateMultiPolicy.person.[4].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-4"
#    ########## QUINTO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[5].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[5].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[5].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[5].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[5].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[5].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[5].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[5].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[5].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[5].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[5].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[5].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-5"
#    And match response.updateMultiPolicy.person.[5].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-5"
#    ########## SEXTO ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[6].PersonPrimerNombre == "OSCAR"
#    And match response.updateMultiPolicy.person.[6].PersonSegundoNombre == "LUIS"
#    And match response.updateMultiPolicy.person.[6].PersonApellidoPaterno == "JUAREZ"
#    And match response.updateMultiPolicy.person.[6].PersonApellidoMaterno == "RAMIREZ"
#    And match response.updateMultiPolicy.person.[6].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[6].PersonFechaNacimiento == "10/07/1993"
#    And match response.updateMultiPolicy.person.[6].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[6].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[6].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[6].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[6].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[6].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-6"
#    And match response.updateMultiPolicy.person.[6].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-6"
#
#     ##### ACTUALIZACIÓN DE 1 ASEGURADO MIGRANTE 2 veces #####
#  Scenario : Actualización de 1 asegurado migrante 2 veces primero sexo y despupes fecha de nacimiento correcto
#    Given path 'v1/update_policy'
#    And request RequestBody1[6]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#    ########## SEGUNDA ACTUALIZACIÓN ASEGURADO ##########
#    And match response.updateMultiPolicy.person.[2].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[2].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[2].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[2].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[2].PersonFechaNacimiento == "01/26/1992"
#    And match response.updateMultiPolicy.person.[2].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[2].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[2].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[2].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[2].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[2].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#
#
#    ##### ACTUALIZACIÓN DE 1 ASEGURADO MIGRANTE EN PERIODO DE CARENCIA #####
#  Scenario : Actualización de 1 asegurado migrante en periodo de carencia
#    Given path 'v1/update_policy'
#    And request RequestBody1[7]
#    When method put
#    Then status 200
#    And print 'Response is: ', response
#    And match response.updateMultiPolicy.person.[1].PersonPrimerNombre == "Carlos"
#    And match response.updateMultiPolicy.person.[1].PersonSegundoNombre == "Alberto"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoPaterno == "Guerrero"
#    And match response.updateMultiPolicy.person.[1].PersonApellidoMaterno == "Rivas"
#    And match response.updateMultiPolicy.person.[1].PersonSexo == "M"
#    And match response.updateMultiPolicy.person.[1].PersonFechaNacimiento == "01/26/1988"
#    And match response.updateMultiPolicy.person.[1].PersonNacionalidad == "74"
#    And match response.updateMultiPolicy.person.[1].PersonCiudadEstado == "ORLANDO"
#    And match response.updateMultiPolicy.person.[1].PersonRfcHomoclave == "PEPE801206"
#    And match response.updateMultiPolicy.person.[1].PersonCurp == "PEPE801206HDFNLG06"
#    And match response.updateMultiPolicy.person.[1].PersonParentescoId == "3"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.NumeroCertificado == "QA100R1001817-1"
#    And match response.updateMultiPolicy.person.[1].EncapsulatedPolicy.PolizaNumero == "05.0102084382084864QA100R1001817-1"
#
#
#    ##### NACIONALIDAD DE CONTRATANTE ASEGURADO DIFERENTE A MEXICANA #####
#  Scenario : Actualizar la nacionalidad del contratabte a una diferente a la mexicana responde code "001-PN"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNacionalidadNoMexicana()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.RetrievePolicyReponse.CodResp contains "001-PN"
#
#
#    ##### NACIONALIDAD DE CONTRATANTE ASEGURADO MIGRABTE DIFERENTE A MEXICANA #####
#  Scenario : Actualizar la nacionalidad del contratabte migrante a una diferente a la mexicana responde code "001-PN"
#    Given path 'v1/update_policy'
#    * def javaMethod = new javaClass().setNacionalidadMigranteNoMexicana()
#    And def bodyRequest = read(userDir + source + 'requestBodyBadUpdate.json')
#    And request RequestBody
#    When method put
#    Then status 400
#    And print 'Response is: ', response
#    And match response.RetrievePolicyReponse.CodResp contains "001-PN"














