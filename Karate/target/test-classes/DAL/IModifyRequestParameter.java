package DAL;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;

import java.io.FileNotFoundException;

public interface IModifyRequestParameter {

    JSONObject getFileSuccessRequest(String pathFile, String absolutePath) throws FileNotFoundException, ParseException;
    void setValueFromKeyIdentifier(String nameOfFile, String pathAbsoluteFilesReq, JSONObject newObject, String nameFileNewReq) throws FileNotFoundException, ParseException;


}
