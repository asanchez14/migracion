Feature: API Cotización
  
  Background:
     * url "http://dummy.restapiexample.com"
       
  Scenario: Validar Code HTTP Code 200
    Given path "/api/v1/create"
    * configure headers = {Content-Type: application/json}
    And request read("data.json")
    When method post
    Then status 200
    Then status 500
    And print "Esto es una impresión en pantalla ", response
    #validation(json)
    And match response.status == "#number", "#notnull"
    And match response.data == "#notnull", "#object"
    And match response.data.name == "#notnull", "#string"
    And match response.data.salary == "#notnull", "#string"
    And match response.data.age == "#notnull", "#string"
    And match response.data.id == "#notnull", "#number"
    And match response.message contains "Successfully! Record has been added."

