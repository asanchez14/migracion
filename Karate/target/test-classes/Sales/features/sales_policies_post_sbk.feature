Feature: Validación de API sales/policies HTTP POST SBK

  Background:
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_sales_policies')
    * url getUrl
    * header Accept = 'application/json'
    * header userId = 'USER'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/Sales/features/requestJSON/'
    * def javaClass = Java.type('utils.SetNumber')

  Scenario: Validar el HTTP code 200 y verificar el Response
    Given path '/api/v1/sales/policies'
    * def javaMethod = new javaClass().setNumberOfPolicies('requestBodySuccessSBK.json')
    And def bodyRequest = read(userDir + source + 'requestBodySuccessSBK.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    And match response == "#object", "#notnull"
    And match response.data.associateId == "#notnull", "#number"
    And match response.data.productId == '#notnull', '#number'
    And match response.data.planId == '#notnull', '#number'
    And match response.data.policy == '#object', '#notnull'
    And match response.data.policy.number == '#notnull', '#string'
    And match response.data.policy.beginValidityDate == '#notnull', '#string'
    And match response.data.policy.endValidityDate == '#notnull', '#string'
    And match response.data.policy.subjectInsured == '#notnull', '#object'
    And match response.data.policy.subjectInsured.subjectDetail == '#array'
    And match response.data.policy.subjectInsured.subjectDetail.[*].givenName == '#notnull'
    And match response.data.policy.subjectInsured.subjectDetail.[*].birthDate == '#notnull'
    And match response.data.policy.subjectInsured.subjectDetail.[*].taxId == '#notnull'
    And match response.data.policy.subjectInsured.subjectDetail.[*].configInsured.relationship == '#notnull'
    And match response.data.policy.subjectInsured.subjectDetail.[*].configInsured.percent == '#notnull'
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary == '#array'
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary.[0].givenName == ['#notnull']
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary.[0].secondName == ['#notnull']
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary.[0].lastName == ['#notnull']
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary.[0].secondLastName == ['#notnull']
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary.[0].birthDate == ['#notnull']
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary.[0].relationship == ['#notnull']
    And match response.data.policy.subjectInsured.subjectDetail.[*].beneficiary.[0].relationship == ['#notnull']
    And print response.data.policy.holder.name
    And match response.data.policy.holder.name == '#notnull', '#string'
    And match response.data.policy.holder.firstName == '#notnull', '#string'
    And match response.data.policy.holder.lastName == '#notnull', '#string'
    And match response.data.policy.holder.taxId == '#notnull', '#string'
    And match response.data.policy.holder.genderId == '#notnull', '#number'
    And match response.data.policy.holder.gender == '#notnull', '#string'
    And match response.data.policy.holder.nationalityId == '#notnull', '#string'
    And match response.data.policy.holder.nationality == '#notnull', '#string'
    And match response.data.policy.holder.birthCountryId == '#notnull', '#string'
    And match response.data.policy.holder.birthCountry == '#notnull', '#string'
    And match response.data.policy.holder.addresses == '#array'
    And match response.data.policy.holder.addresses.[*].street == '#notnull'
    And match response.data.policy.holder.addresses.[*].externalNumber == '#notnull'
    And match response.data.policy.holder.addresses.[*].postalCode == '#notnull'
    And match response.data.policy.holder.addresses.[*].cityId == '#notnull'
    And match response.data.policy.holder.addresses.[*].townshipId == '#notnull'
    And match response.data.policy.holder.addresses.[*].suburbId == '#notnull'
    And match response.data.policy.holder.addresses.[*].suburb == '#notnull'
    And match response.data.policy.holder.addresses.[*].country == '#notnull'
    And match response.data.payment.typeId == '#notnull', '#string'
    And match response.data.payment.totalAmount == '#object'
    And match response.data.payment.totalAmount.amount == '#notnull', '#number'
    And match response.data.payment.totalAmount.currency == '#notnull', '#string'
    And match response.data.channels.[0].channelNotificationId == '#number'
    And match response.data.channels.[0].channelNotification == '#string'
    And match response.data.extraFields == '#present'

  Scenario: Validar HTTP code 500
    Given path '/api/v1/sales/policies'
    And def bodyRequest = read(userDir + source + 'requestBodySuccess.json')
    And request bodyRequest
    When method post
    Then status 500
    And print "Validate HTTP Code 500 response: ", response

  Scenario: Validar HTTP code 400 sin enviar userId en header
    Given path '/api/v1/sales/policies'
    * header userId = ''
    And def bodyRequest = read(userDir + source + 'requestBodySuccessSBK.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response









