package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;

public class CallMethodCancel {

    ModifyRequestParameterImpl modify = new ModifyRequestParameterImpl();
    String nameOfFileSuccess = "requestBodySuccessCancel.json";
    String nameOfFileBadRequest = "requestBodyBadCancel.json";

    public JSONObject getAllObject() throws FileNotFoundException, ParseException {
        return modify.getFileSuccessRequest(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON);
    }

    /// NO. PÓLIZA //
    public void setNoPoliciesConCertificate() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancelPolicy = (JSONObject) json.get("cancelPolicy");
        cancelPolicy.put("NumerosPolizas","QA100R2001839-2");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoPoliciesBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancelPolicy = (JSONObject) json.get("cancelPolicy");
        cancelPolicy.put("NumerosPolizas", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoPoliciesLargoCharacterEspecial() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancel = (JSONObject) json.get("cancelPolicy");
        cancel.put("NumerosPolizas", "@@@@@@@@@@####$%&#4QA100R700181425475-1");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoPoliciesNoExist() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancel = (JSONObject) json.get("cancelPolicy");
        cancel.put("NumerosPolizas", "05.0102084383084844QA100R70018145-1");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NO. SUCURSAL ///
    public void setNoSubsidiaryBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancel = (JSONObject) json.get("cancelPolicy");
        cancel.put("NumeroSucursal", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoSubsidiaryLargoCharacterEspecial() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancel = (JSONObject) json.get("cancelPolicy");
        cancel.put("NumeroSucursal", "Q#$%&/00000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// ID USUARIO ///
    public void setIdUserLargoCharactersEspecial() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancel = (JSONObject) json.get("cancelPolicy");
        cancel.put("IdUsuario", "Q#$%&/00000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setIdUserBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject cancel = (JSONObject) json.get("cancelPolicy");
        cancel.put("IdUsuario", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_CANCEL_REQUEST_JSON,json, nameOfFileBadRequest);
    }
}
