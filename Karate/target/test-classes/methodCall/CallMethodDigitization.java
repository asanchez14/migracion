package methodCall;

import DAL.ModifyRequestParameterImpl;
import jodd.json.meta.JSON;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;

public class CallMethodDigitization {
    ModifyRequestParameterImpl modify;
    static String jsonDigitizationSuccess = "requestBodyDigitizationSuccessPUT.json";


    public CallMethodDigitization() throws FileNotFoundException, ParseException {
        modify = new ModifyRequestParameterImpl();
    }


    public String getNumberOfPolicyRegisteredForDigitization() throws FileNotFoundException, ParseException {
        JSONObject numberPolicy = new ModifyRequestParameterImpl().getFileSuccessRequest("requestBodySuccess.json", Constants.PATH_SALES_REQUEST_JSON);
        JSONObject policy = (JSONObject) numberPolicy.get("policy");
        return policy.get("number").toString();
    }

    public void setFortmat() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON);
        JSONArray documents = (JSONArray) jsonObject.get("documents");
        JSONObject documentsObject = (JSONObject) documents.get(0);
        documentsObject.put("format", "PDFF");
        modify.setValueFromKeyIdentifier(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyDigitizationBad.json");
    }

    public void setFortmatNull() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON);
        JSONArray documents = (JSONArray) jsonObject.get("documents");
        JSONObject documentsObject = (JSONObject) documents.get(0);
        documentsObject.put("format", null);
        modify.setValueFromKeyIdentifier(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyDigitizationBad.json");
    }

    public void setTypeNull() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON);
        JSONArray documents = (JSONArray) jsonObject.get("documents");
        JSONObject documentsObject = (JSONObject) documents.get(0);
        documentsObject.put("type", null);
        modify.setValueFromKeyIdentifier(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyDigitizationBad.json");
    }

    public void setValueNull() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON);
        JSONArray documents = (JSONArray) jsonObject.get("documents");
        JSONObject documentsObject = (JSONObject) documents.get(0);
        documentsObject.put("value", null);
        modify.setValueFromKeyIdentifier(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyDigitizationBad.json");
    }

    public void setType() throws FileNotFoundException, ParseException {
        JSONObject jsonObject = modify.getFileSuccessRequest(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON);
        JSONArray documents = (JSONArray) jsonObject.get("documents");
        JSONObject documentsObject = (JSONObject) documents.get(0);
        documentsObject.put("type", "test");
        modify.setValueFromKeyIdentifier(jsonDigitizationSuccess, Constants.PATH_PARTNER_REQUEST_JSON,jsonObject, "requestBodyDigitizationBad.json");
    }
}
