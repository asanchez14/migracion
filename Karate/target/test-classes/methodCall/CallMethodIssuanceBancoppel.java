package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CallMethodIssuanceBancoppel {

    ModifyRequestParameterImpl modify = new ModifyRequestParameterImpl();
    String nameOfFileSuccess = "requestBoddySuccesBancoppel.json";
    String nameOfFileBadRequest = "requestBodyBadBancoppel.json";

    public JSONObject getAllObject() throws FileNotFoundException, ParseException {
        return modify.getFileSuccessRequest(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES);
    }

    public void setNumeroCertificadoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        policy.put("certificateNumber","@@@@@@#$%&123456789012345678901");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setNumeroCertificadoNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        policy.put("certificateNumber",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// SUCURSAL ///
    public void setCanalLargoCaractaresEspecialesLetras() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject canal = (JSONObject) json.get("channel");
        canal.put("canalId", "@@@ADFG1234");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setCanalNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject canal = (JSONObject) json.get("channel");
        canal.put("canalId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// ID USUARIO ///
    public void setIdUsuarioLargoCaractaresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject IdAgente = (JSONObject) json.get("agent");
        IdAgente.put("id", "@@@#$%&/()/4231746387912649781362748632178653476457");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setIdUsuarioNull() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject IdAgente = (JSONObject) json.get("agent");
        IdAgente.put("agent", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// PRIMER NOMBRE ///
    public void setPrimerNombreLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("givenName", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("givenName", "José-Toño");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("givenName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// SEGUNDO NOMBRE ///
    public void setSegundoNombreLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("otherGivenName", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("otherGivenName", "José-Toño");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("otherGivenName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// APELLIDO PATERNO ///
    public void setApellidoPaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("surname", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("surname", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("surname", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// APELLIDO MATERNO ///
    public void setApellidoMaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("otherSurname", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("otherSurname", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("otherSurname", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

         /// SEXO ///
    public void setSexoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("gender", "@2F");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setSexoF() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("gender", "F");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setSexoM() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("gender", "M");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setSexoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("gender", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// FECHA NACIMIENTO ///
    public void setFechaNacimientoLargaFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthDate", "1984/12/122");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMayorActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthDate", "12/12/2021");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMayorDeLoPermitido() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthDate", "01/01/1940");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMenorDeEdad() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthDate", "01/01/2010");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthDate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// ESTADO DE NACIMIENTO ///
    public void setEstadoNacimientoLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthCountryId", "@A11");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setEstadoNacimientoNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthCountryId", "35");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setEstadoNacimientoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthCountryId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// CALLE ///
    public void setCalleLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("addressName", "@@@@@#$%&/5465465464654654654654654654654");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setCalleNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("addressName", "9999999");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setCalleBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("addressName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// NO.EXTERIOR ///
    public void setNoExteriorLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("externalNumber", "@#$%&55555555555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES, json, nameOfFileBadRequest);
    }

    public void setNoExteriorBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("externalNumber", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// NO.INTERIOR ///
    public void setNoInteriorLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("internalNumber", "@#$%&55555555555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES, json, nameOfFileBadRequest);
    }

    public void setNoInteriorBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("internalNumber", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// CÓDIGO POSTAL ///
    public void setCPLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("postalCode", "@@5465");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setCPNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("postalCode", "00001");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setCPBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("postalCode", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// COLONIA ///
    public void setColoniaLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("suburbId", "@#$%2222222");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setColoniaNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("suburbId", "49222");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setColoniaBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("suburbId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// DELEGACIÓN / MUNICIPIO ///
    public void setMunicipioLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("townshipId", "@#A55");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setMunicipioBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("townshipId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// CIUDAD / ESTADO ///
    public void setCiudadLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("cityId", "@@@#$%&/()12312345678978945641232145874125874512452");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setCiudadNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("cityId", "99");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setCiudadBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray adresss = (JSONArray) holder.get("addresses");
        JSONObject adresssObject = (JSONObject) adresss.get(0);
        adresssObject.put("cityId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// RFC ///
    public void setRFCLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("taxId", "@@#$55121654646565R9");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setRFCConLetraN() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("taxId", "GAÑE551210R91");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setRFCBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("taxId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// TELÉFONO ///
    public void setTelefonoLetrasCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray phone = (JSONArray) holder.get("phones");
        JSONObject phoneObject = (JSONObject) phone.get(0);
        phoneObject.put("number", "AA/(%&/(%&");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setTelefonoLargo() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray phone = (JSONArray) holder.get("phones");
        JSONObject phoneObject = (JSONObject) phone.get(0);
        phoneObject.put("number", "5555546546546555555555464646546");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setTelefonoCorto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray phone = (JSONArray) holder.get("phones");
        JSONObject phoneObject = (JSONObject) phone.get(0);
        phoneObject.put("number", "555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setTelefonoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray phone = (JSONArray) holder.get("phones");
        JSONObject phoneObject = (JSONObject) phone.get(0);
        phoneObject.put("number", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// EMAIL ///
    public void setEmailCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", "art&&%$%/)=/_612@hotmail.com");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setEmailSinArroba() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", "art_612hotmail.com");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setEmailSinDominio() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", "art_612hotmailcom");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setEmailBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setEmailCaracteresEspecialesPermitidos() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", "art_6-12@hotmail.com");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    ////////// DATOS ASEGURADO MIGRANTE //////////

    /// PRIMER NOMBRE ///
    public void setPrimerNombreMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject nameObject = (JSONObject) subjectDetail.get(0);
        nameObject.put("givenName", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject nameObject = (JSONObject) subjectDetail.get(0);
        nameObject.put("givenName", "José-Toño");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject nameObject = (JSONObject) subjectDetail.get(0);
        nameObject.put("givenName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// SEGUNDO NOMBRE ///
    public void setSegundoNombreMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject secondNameObject = (JSONObject) subjectDetail.get(0);
        secondNameObject.put("secondName", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject secondNameObject = (JSONObject) subjectDetail.get(0);
        secondNameObject.put("secondName", "José-Toño");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject secondNameObject = (JSONObject) subjectDetail.get(0);
        secondNameObject.put("secondName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// APELLIDO PATERNO ///
    public void setApellidoPaternoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject paternoObject = (JSONObject) subjectDetail.get(0);
        paternoObject.put("lastName", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject paternoObject = (JSONObject) subjectDetail.get(0);
        paternoObject.put("lastName", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject paternoObject = (JSONObject) subjectDetail.get(0);
        paternoObject.put("lastName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// APELLIDO MATERNO ///
    public void setApellidoMaternoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject maternoObject = (JSONObject) subjectDetail.get(0);
        maternoObject.put("secondLastName", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject maternoObject = (JSONObject) subjectDetail.get(0);
        maternoObject.put("secondLastName", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject maternoObject = (JSONObject) subjectDetail.get(0);
        maternoObject.put("secondLastName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// FECHA INICIO SEGURO ///
    public void getDate() throws FileNotFoundException, ParseException{
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        String date = sdf.format(new Date());
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject inicioSeguro = (JSONObject) polici.get("policy");
        inicioSeguro.put("beginValidityDate", date);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
        System.out.println(date);

    }
    public void setFechaInicioSeguroBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject inicioSeguro = (JSONObject) polici.get("policy");
        inicioSeguro.put("beginValidityDate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setFechaInicioSeguroFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject inicioSeguro = (JSONObject) polici.get("policy");
        inicioSeguro.put("beginValidityDate", "2020/09/060");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setFechaInicioSeguroMayorDiaActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject inicioSeguro = (JSONObject) polici.get("policy");
        inicioSeguro.put("beginValidityDate", "10/03/2022");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setFechaInicioSeguroMenorDiaActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject inicioSeguro = (JSONObject) polici.get("policy");
        inicioSeguro.put("beginValidityDate", "10/02/2020");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

      /// PERIODO DE PAGO ///
    public void setPeriodoDePagoCorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject perido = (JSONObject) polici.get("policy");
        perido.put("term", "4");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPeriodoPagoCaracteresEspecialesLargo() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject perido = (JSONObject) polici.get("policy");
        perido.put("term", "@6A5");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPeriodoPagoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject perido = (JSONObject) polici.get("policy");
        perido.put("term", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPeriodoDePago6Meses() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject perido = (JSONObject) polici.get("policy");
        perido.put("term", "5");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPeriodoDePago12Meses() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject perido = (JSONObject) polici.get("policy");
        perido.put("term", "4");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    /// PRIMA PÓLIZA ///
    public void setPrimaPolizaLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject prima = (JSONObject) polici.get("policy");
        prima.put("premiumValue", "@#$AA546544.00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimaPolizaCorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject prima = (JSONObject) polici.get("policy");
        prima.put("premiumValue", "240.00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimaPolizaBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject prima = (JSONObject) polici.get("policy");
        prima.put("premiumValue", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimaPolizaNoCoincideConPeriodoPago4() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject prima = (JSONObject) polici.get("policy");
        prima.put("premiumValue", "950.00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimaPolizaNoCoincideConPeriodoPago5() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject prima = (JSONObject) polici.get("policy");
        prima.put("premiumValue", "840.00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimaPolizaNoCoincideConPeriodoPago6() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject prima = (JSONObject) polici.get("policy");
        prima.put("premiumValue", "120.00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

    public void setPrimaPolizaNoCoincideConPeriodoPago12() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject polici = (JSONObject) subjectDetail.get(0);
        JSONObject prima = (JSONObject) polici.get("policy");
        prima.put("premiumValue", "240.00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.ISSUANCE_REQUEST_POLICIES,json, nameOfFileBadRequest);
    }

}
