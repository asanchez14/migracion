package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;

public class CallMethodRenewal {

    ModifyRequestParameterImpl modify = new ModifyRequestParameterImpl();
    String nameOfFileSuccess = "requestBodySuccessRenewal.json";
    String nameOfFileBadRequest = "requestBodyBadRenewal.json";

    public JSONObject getAllObject() throws FileNotFoundException, ParseException {
        return modify.getFileSuccessRequest(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON);
    }

    /// NO. SUCURSAL ///
    public void setNumeroSucursalLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("idStore", "@#$73465873");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNumeroSucursalBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("idStore", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// ID USUARIO ///
    public void setIdUsuarioLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("userModify", "@#$@#$%&UO0020000000000000000000000000000000000000000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setIdUsuarioBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("userModify", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// PRIMER NOMBRE ///
    public void setPrimerNombreLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("name", "@#$@#$%&UO0020000000000000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("name", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// SEGUNDO NOMBRE ///
    public void setSegundoNombreLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("secondname", "@#$@#$%&UO0020000000000000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSegudnoNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("secondname", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO PATERNO ///
    public void setApellidoPaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("lastName", "@#$@#$%&@@JUAREZJUAREZJUREZ");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("lastName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO MATERNO ///
    public void setApellidoMaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("secondlastName", "@#$@#$%&@@JUAREZJUAREZJUREZ");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("secondlastName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// FECHA DE NACIMIENTO ///
    public void setFechaNacimientoLargoFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("birthdate", "1945/12/100");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("birthdate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMayorActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("birthdate", "31/12/2021");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMayorPermitida() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("birthdate", "31/12/1940");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMenorPermitida() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("birthdate", "31/12/2010");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// TELÉFONO ///
    public void setTelefonoCaracteresEspecialesLetras() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("PersonTelefonoCelular", "@#$@#AAAAA");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("PersonTelefonoCelular", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMenosDigitos() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("PersonTelefonoCelular", "523");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMasDigitos() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject person = (JSONObject) json.get("client");
        person.put("PersonTelefonoCelular", "5578894556123014789456320154789");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CERTIFICADO ///
    public void setCertificadoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("certificate", "@#$@@@#$%&457852012587452012544");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCertificadoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        json.put("certificate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

                                              /// DATOS MIGRABTE ///

    /// PRIMER NOMBRE ASEGURADO MIGRANTE///
    public void setPrimerNombreMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("name", "@#$@#$%&UO0020000000000000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("name", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// SEGUNDO NOMBRE ASEGURADO MIGRANTE///
    public void setSegundoNombreMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("Secondname", "@#$@#$%&UO0020000000000000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("Secondname", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO PATERNO ///
    public void setApellidoPaternoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("lastName", "@#$@#$%&@@JUAREZJUAREZJUREZ");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("lastName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO MATERNO ///
    public void setApellidoMaternoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("secondlastName", "@#$@#$%&@@JUAREZJUAREZJUREZ");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("secondlastName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// FECHA DE NACIMIENTO ///
    public void setFechaNacimientoMigranteLargoFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("birthdate", "1945/12/100");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("birthdate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMigranteMayorActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("birthdate", "31/12/2021");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMigranteMayorPermitida() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("birthdate", "31/12/1940");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMigranteMenorPermitida() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("birthdate", "31/12/2010");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// TELÉFONO ///
    public void setTelefonoMigranteCaracteresEspecialesLetras() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("PersonTelefonoCelular", "@#$@#AAAAA");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("PersonTelefonoCelular", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMigranteMenosDigitos() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("PersonTelefonoCelular", "523");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMigranteMasDigitos() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("PersonTelefonoCelular", "5578894556123014789456320154789");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CERTIFICADO ///
    public void setCertificadoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("certificate", "@#$@@@#$%&457852012587452012544");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCertificadoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("certificate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// FECHA INICIO SEGURO ///
    public void setFechaInicioSeguroLargoFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("beginValidityDate", "2021/03/177");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaInicioSeguroBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("beginValidityDate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaInicioSeguroMayorActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("beginValidityDate", "31/12/2021");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// FECHA FIN SEGURO ///
    public void setFechaFinSeguroLargoFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("endValidityDate", "2021/03/177");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaFinSeguroBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("endValidityDate", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaFinSeguroMayorActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        JSONObject beneficiari = (JSONObject) policyObject.get("beneficiary");
        beneficiari.put("endValidityDate", "31/12/2021");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// PERIODO DE PAGO ///
    public void setPeriodoPagoLetrasCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("renewalPeriod", "@A");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPeriodoPagoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("renewalPeriod", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPeriodoPagoLargo() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("renewalPeriod", "444");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPeriodoPagoNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("renewalPeriod", "10");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// PRIMA PÓLIZA ///
    public void setPrimaPolizaIncorrecta() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("PrimaPoliza", "120");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPrimaPolizaBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONArray policies = (JSONArray) json.get("policies");
        JSONObject policyObject = (JSONObject) policies.get(0);
        policyObject.put("PrimaPoliza", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_RENEWAL_REQUEST_JSON,json, nameOfFileBadRequest);
    }
}
