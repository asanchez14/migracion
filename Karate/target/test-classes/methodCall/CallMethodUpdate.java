package methodCall;

import DAL.ModifyRequestParameterImpl;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.ParseException;
import utils.Constants;

import java.io.FileNotFoundException;

public class CallMethodUpdate {

    ModifyRequestParameterImpl modify = new ModifyRequestParameterImpl();
    String nameOfFileSuccess = "requestBodySuccessUpdate.json";
    String nameOfFileBadRequest = "requestBodyBadUpdate.json";

    public JSONObject getAllObject() throws FileNotFoundException, ParseException {
        return modify.getFileSuccessRequest(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON);
    }

    public void setNoSubsidiaryLargoCharactersEspecial() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        Multipolicy.put("NoSucursal","@#$73465873");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoSubsidiaryBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        Multipolicy.put("NoSucursal",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

     /// ID USUARIO ///
    public void setIdUserLargoCharactersEspecialLetters() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Idusuario = (JSONObject) json.get("updateMultiPolicy");
        Idusuario.put("IdUsuario","@#$%&0593429587348570839475389247508273458734280957");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setIdUserBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Idusuario = (JSONObject) json.get("updateMultiPolicy");
        Idusuario.put("IdUsuario",null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// PRIMER NOMBRE ///
    public void setFirstNameLargoCharacterEspecial() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonPrimerNombre", "@@@@@@@@@@#$%&#$%&#$%&#$%&#");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPrimerNameBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonPrimerNombre", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPrimerNameAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonPrimerNombre", "José-Toño");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// SEGUNDO NOMBRE ///
    public void setSegundoNombreLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonSegundoNombre", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonSegundoNombre", "Toño de Jesús");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonSegundoNombre", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO PATERNO ///
    public void setApellidoPaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonApellidoPaterno", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonApellidoPaterno", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonApellidoPaterno", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO MATERNO ///
    public void setApellidoMaternoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonApellidoMaterno", "JUAREZzzzzzzzzzzzzzz@@@$%&/");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonApellidoMaterno", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonApellidoMaterno", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// SEXO ///
    public void setSexoLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonSexo", "@2A");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSexoF() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonSexo", "F");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSexoM() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonSexo", "M");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSexoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonSexo", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// FECHA NACIMIENTO ///
    public void setFechaNacimientoLargaFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonFechaNacimiento", "1988/26/011");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMayorActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonFechaNacimiento", "12/12/2021");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMayorDeLoPermitido() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonFechaNacimiento", "01/01/1940");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMenorDeEdad() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonFechaNacimiento", "01/01/2010");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonFechaNacimiento", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// ESTADO DE NACIMIENTO ///
    public void setEstadoNacimientoLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        holder.put("birthCountryId", "@A11");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEstadoNacimientoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonEstadoNacimiento", "@S");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEstadoNacimientoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonEstadoNacimiento", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEstadoNacimientoNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonEstadoNacimiento", "74");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NACIONALIDAD ///
    public void setNacionalidadLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNacionalidad", "@#ES");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNacionalidadBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNacionalidad", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNacionalidadNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNacionalidad", "00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNacionalidadNoMexicana() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNacionalidad", "73");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CALLE ///
    public void setCalleLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCalle", "@@@@@#$%&/5465465464654654654654654654654");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCalleNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCalle", "9999999");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCalleBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCalle", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NO.EXTERIOR ///
    public void setNoExteriorLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNumeroExterior", "@#$%&55555555555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoExteriorBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNumeroExterior", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NO.INTERIOR ///
    public void setNoInteriorLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNumeroInterior", "@#$%&55555555555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoInteriorBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonNumeroInterior", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CÓDIGO POSTAL ///
    public void setCPLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCP", "@@5465");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCPNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCP", "00001");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCPBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCP", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// COLONIA ///
    public void setColoniaLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonColonia", "@#$%2222222");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setColoniaNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonColonia", "49222");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setColoniaBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonColonia", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// DELEGACIÓN / MUNICIPIO ///
    public void setMunicipioLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonDelegacionMunicipio", "@#A55");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setMunicipioBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonDelegacionMunicipio", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CIUDAD / ESTADO ///
    public void setCiudadLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCiudadEstado", "@@@#$%&/()12312345678978945641232145874125874512452");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCiudadNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCiudadEstado", "99");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCiudadBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonCiudadEstado", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// TELÉFONO ///
    public void setTelefonoLetrasCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonTelefonoCelular", "AA/(%&/(%&");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoLargo() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonTelefonoCelular", "5555546546546555555555464646546");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoCorto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonTelefonoCelular", "555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonTelefonoCelular", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// EMAIL ///
    public void setEmailCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", "art&&%$%/)=/_612@hotmail.com");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEmailSinArroba() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", "art_612hotmail.com");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEmailSinDominio() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject holder = (JSONObject) policy.get("holder");
        JSONArray correo = (JSONArray) holder.get("emails");
        JSONObject correoObject = (JSONObject) correo.get(0);
        correoObject.put("address", "art_612hotmailcom");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEmailBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonEmail", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEmailCaracteresEspecialesPermitidos() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        personObject.put("PersonEmail", "art_6-12@hotmail.com");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NÚMERO CERTIFICADO ///
    public void setNumeroCertificadoLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("NumeroCertificado", "@@#$%r0070000000000000000000000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNumeroCertificadoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("NumeroCertificado", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NÚMERO PÓLIZA ///
    public void setNumeroPolizaLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("PolizaNumero", "@#$%&/()%$02120589896325874520125856");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNumeroPolizaBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("PolizaNumero", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    ////////// DATOS ASEGURADO MIGRANTE //////////

    /// PRIMER NOMBRE ///
    public void setPrimerNombreMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonPrimerNombre", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);

    }

    public void setPrimerNombreMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject nameObject = (JSONObject) subjectDetail.get(0);
        nameObject.put("givenName", "José-Toño");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setPrimerNombreMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject nameObject = (JSONObject) subjectDetail.get(0);
        nameObject.put("givenName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// SEGUNDO NOMBRE ///
    public void setSegundoNombreMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonSegundoNombre", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonSegundoNombre", "Toño de Jesús");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setSegundoNombreMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonSegundoNombre", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO PATERNO ///
    public void setApellidoPaternoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonApellidoPaterno", "@#$%&SSSSSSSSSSSSSSSSSSSSSS");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject paternoObject = (JSONObject) subjectDetail.get(0);
        paternoObject.put("lastName", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoPaternoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject policy = (JSONObject) json.get("policy");
        JSONObject subjectInsured = (JSONObject) policy.get("subjectInsured");
        JSONArray subjectDetail = (JSONArray) subjectInsured.get("subjectDetail");
        JSONObject paternoObject = (JSONObject) subjectDetail.get(0);
        paternoObject.put("lastName", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// APELLIDO MATERNO ///
    public void setApellidoMaternoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonApellidoMaterno", "JUAREZzzzzzzzzzzzzzz@@@$%&/");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoMigranteAcentoCompuesto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonApellidoMaterno", "Riaño López");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setApellidoMaternoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonApellidoMaterno", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

        /// SEXO ///
    public void setSexoMigranteLargoCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonSexo", "@2A");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setMigranteSexoF() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonSexo", "F");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setMigranteSexoM() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonSexo", "M");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setMigranteSexoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonSexo", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// FECHA DE NACIMIENTO ///
    public void setFechaNacimientoMigranteLargaFormatoIncorrecto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonFechaNacimiento", "1988/26/011");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMigranteMayorActual() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonFechaNacimiento", "12/12/2021");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMayorMigranteDeLoPermitido() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonFechaNacimiento", "01/01/1940");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMigranteMenorDeEdad() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonFechaNacimiento", "01/01/2010");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setFechaNacimientoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonFechaNacimiento", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// ESTADO DE NACIMIENTO ///
    public void setEstadoNacimientoMigranteCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonEstadoNacimiento", "@S");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEstadoNacimientoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonEstadoNacimiento", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEstadoNacimientoMigranteNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonEstadoNacimiento", "74");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NACIONALIDAD ///
    public void setNacionalidadMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNacionalidad", "@#ES");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNacionalidadMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNacionalidad", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNacionalidadMigranteNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNacionalidad", "00");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNacionalidadMigranteNoMexicana() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNacionalidad", "73");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CALLE ///
    public void setCalleMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCalle", "@@@@@#$%&/5465465464654654654654654654654");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCalleMigranteNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCalle", "9999999");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCalleMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCalle", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.UPDATE_POLICIES,json, nameOfFileBadRequest);
    }

    /// NO.EXTERIOR ///
    public void setNoExteriorMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNumeroExterior", "@#$%&55555555555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoExteriorMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNumeroExterior", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NO.INTERIOR ///
    public void setNoInteriorMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNumeroInterior", "@#$%&55555555555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNoInteriorMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonNumeroInterior", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CÓDIGO POSTAL ///
    public void setCPMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCP", "@@5465");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCPMigranteNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCP", "00001");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCPMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCP", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// COLONIA ///
    public void setColoniaMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonColonia", "@#$%2222222");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setColoniaMigranteNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonColonia", "49222");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setColoniaMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonColonia", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// DELEGACIÓN / MUNICIPIO ///
    public void setMunicipioMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonDelegacionMunicipio", "@#A55");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setMunicipioMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonDelegacionMunicipio", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CIUDAD / ESTADO ///
    public void setCiudadMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCiudadEstado", "@@@#$%&/()12312345678978945641232145874125874512452");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCiudadMigranteNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCiudadEstado", "99");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCiudadMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCiudadEstado", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// TELÉFONO ///
    public void setTelefonoMigranteLetrasCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonTelefonoCelular", "AA/(%&/(%&");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMigranteLargo() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonTelefonoCelular", "5555546546546555555555464646546");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMigranteCorto() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonTelefonoCelular", "555");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setTelefonoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonTelefonoCelular", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// EMAIL ///
    public void setEmailMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonEmail", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setEmailMigranteCaracteresEspecialesPermitidos() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonEmail", "art_6-12@hotmail.com");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// RFC ///
    public void setRFCLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonRfcHomoclave", "@@#$55121654646565R9");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setRFCBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonRfcHomoclave", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setRFCConLetraN() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonRfcHomoclave", "GAÑE551210R91");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// CURP ///
    public void setCURPLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCurp", "@@@@801206HDFNLG048");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCURPBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCurp", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setCURPConLetraN() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonCurp", "GAÑE551210R91");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// PARENTESCO MIGRANTE ///
    public void setParentescoLarga() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonParentescoId", "333");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setParentescoBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonParentescoId", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setParentescoNoRegistrado() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        personObject.put("PersonParentescoId", "99");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NÚMERO CERTIFICADO MIGRANTE ///
    public void setNumeroCertificadoMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("NumeroCertificado", "@@#$%r0070000000000000000000000");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNumeroCertificadoMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(1);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("NumeroCertificado", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    /// NÚMERO PÓLIZA ///
    public void setNumeroPolizaMigranteLargaCaracteresEspeciales() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("PolizaNumero", "@#$%&/()%$02120589896325874520125856");
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }

    public void setNumeroPolizaMigranteBlanco() throws FileNotFoundException, ParseException {
        JSONObject json = getAllObject();
        JSONObject Multipolicy = (JSONObject) json.get("updateMultiPolicy");
        JSONArray person = (JSONArray) Multipolicy.get("Person");
        JSONObject personObject = (JSONObject) person.get(0);
        JSONObject Policy = (JSONObject) personObject.get("EncapsulatedPolicy");
        Policy.put("PolizaNumero", null);
        modify.setValueFromKeyIdentifier(nameOfFileSuccess, Constants.PATH_UPDATE_REQUEST_JSON,json, nameOfFileBadRequest);
    }



}
