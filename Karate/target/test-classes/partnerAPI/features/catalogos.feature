Feature: Catalogos

  Background:
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_catalogs')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def call = Java.type('methodCall.CallMethodCatalog')
    * def source = '/src/test/java/partnerAPI/features/request/'

  Scenario: Validar la API /catalogs correcto y Validar el response correcto
    * url getUrl + '/api/v1/catalogs?associate_name=Wolkswagen+Mexico&page_number=0&page_size=1'
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data == '#array'
    And match response.data.[*].businessLine == '#object', '#present'
    And match response.data.[*].associated == '#object', '#present'
    And match response.data.[*].businessLine.name == '#string', '#present'
    And match response.data.[*].businessLine.id == '#string', '#present'
    And match response.data.[*].associated.name == '#string', '#present'
    And match response.data.[*].associated.id == '#string', '#present'
    And match response.data.[*].name == '#string', '#present'
    And match response.data.[*].description == '#string', '#present'
    And match response.data.[*].active == '#boolean', '#present'
    And match response.data.[*].id == '#number', '#present'
    And match response.notifications == '#array', '#present'

  Scenario: Validar con un page_number consecutivo
    * url getUrl + '/api/v1/catalogs?associate_name=Wolkswagen+Mexico&page_number=1&page_size=1'
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data == '#array'
    And match response.data.[*].businessLine == '#object', '#present'
    And match response.data.[*].associated == '#object', '#present'
    And match response.data.[*].businessLine.name == '#string', '#present'
    And match response.data.[*].businessLine.id == '#string', '#present'
    And match response.data.[*].associated.name == '#string', '#present'
    And match response.data.[*].associated.id == '#string', '#present'
    And match response.data.[*].name == '#string', '#present'
    And match response.data.[*].description == '#string', '#present'
    And match response.data.[*].active == '#boolean', '#present'
    And match response.data.[*].id == '#number', '#present'
    And match response.notifications == '#array', '#present'

  Scenario: Validar con un page_number excedido mande campos vacíos
    * url getUrl + '/api/v1/catalogs?associate_name=Wolkswagen+Mexico&page_number=500&page_size=1'
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data == '#array'
    And match response.notifications == '#array', '#present'

  Scenario: Validar con un page_size 0
    * url getUrl + '/api/v1/catalogs?associate_name=Wolkswagen+Mexico&page_number=500&page_size=0'
    When method get
    Then status 500
    * print "Response: ", response
    And match response.error contains 'Internal Server Error'

  Scenario: Validar con un page_size excedente
    * url getUrl + '/api/v1/catalogs?associate_name=Wolkswagen+Mexico&page_number=1&page_size=100'
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data == '#array'
    And match response.notifications == '#array', '#present'

  Scenario: Validar un associated_name inexistente
    * url getUrl + '/api/v1/catalogs?associate_name=TestInexistente&page_number=1&page_size=1'
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data == '#array'
    And match response.notifications == '#array', '#present'

  Scenario: Validar sin payload request
    * url getUrl + '/api/v1/catalogs'
    When method get
    Then status 400

  Scenario: Validar un path incorrecto plans
    * url getUrl + '/api/v1/catalogs'
    When method get
    Then status 400

  Scenario: Validar el response completo y datos obligatorios plans
    * url getUrl + '/api/v1/catalogs/plans'
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data == '#array', '#present'
    And match response.notifications == '#array', '#present'
    And match response.data.[*].name == '#string', '#notnull'
    And match response.data.[*].description == '#string', '#notnull'
    And match response.data.[*].items == '#array', '#notnull'
    And match response.data.[*].items.[*].id == '#number', '#present'
    And match response.data.[*].items.[*].name == '#string', '#notnull'
    And match response.data.[*].items.[*].status == '#string', '#present'
    And match response.notifications == '#array', '#present'
    And match response.notifications.[0].code == '#number', '#present'
    And match response.notifications.[0].message == '#string', '#present'

  Scenario: Validar un path incorrecto Get Catalogs  by Associated and Bussines
    * url getUrl + '/api/v1/catalogs/associates'
    When method get
    Then status 400

  Scenario: Validar el response completo y datos obligatorios Get Catalogs  by Associated and Bussines
    * url getUrl + '/api/v1/catalogs/associates?associate_id=1&business_line=1'
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data == '#array', '#present'
    And match response.notifications == '#array', '#present'
    And match response.data.[*].id == '#string', '#notnull'
    And match response.data.[*].name == '#string', '#notnull'
    And match response.data.[*].description == '#string', '#notnull'
    And match response.data.[*].businessLineId == '#string', '#notnull'
    And match response.data.[*].active == '#string', '#notnull'
    And match response.data.[*].items == '#array', '#notnull'
    And match response.data.[*].items.[*].key == '#present'
    And match response.data.[*].items.[*].value == '#present'
    And match response.data.[*].items.[*].subcatalog == '#present'
    And match response.data.[*].items.[*].id == '#present'
    And match response.data.[*].items.[*].active == '#present'

  Scenario: Validar el mayor número de carácteres en name request body Crear catalogo
    Given path '/api/v1/catalogs'
    * def javaMethod = new call().setName('qwertyuiopqwertyuiopasdfghjklñzxcvbnmqwertgffgghhrignjtritrngitrngrt')
    And def bodyRequest = read(userDir + source + 'requestBodyCatalogNewFile.json')
    And request bodyRequest
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications == '#array', '#present'
    And match response.notifications.[0].code == '#number', '#present', 400
    And match response.notifications.[0].message contains 'name - size must be between 0 and 40'

  Scenario: Validar que no venga nulo el campo name Crear catalogo
    Given path '/api/v1/catalogs'
    And def bodyRequest = read(userDir + source + 'requestBodyCatalogNewFile.json')
    * def javaMethod = new call().setName('')
    And request bodyRequest
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications == '#array', '#present'
    And match response.notifications.[0].code == '#number', '#present', 400

  Scenario: Validar que no venga vacio campo description Crear catalogo
    Given path '/api/v1/catalogs'
    And def bodyRequest = read(userDir + source + 'requestBodyCatalogNewFile.json')
    * def javaMethod = new call().setDescription('')
    And request bodyRequest
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications == '#array', '#present'
    And match response.notifications.[0].code == '#number', '#present', 400
    And match response.notifications.[0].message contains 'name - must not be blank'

  Scenario: Validar que no venga nulo bussinessLineId Crear catalogo
    Given path '/api/v1/catalogs'
    * def req =
    """
        {
            "description": "Catalog for testing purpose",
            "name": "TEST_CATALOG_TEST_MIGRACION",
            "items": [
                {
                    "key": "1",
                    "value": "Uno",
                    "subcatalog": []
                },
                {
                    "key": "2",
                    "value": "Dos",
                    "subcatalog": []
                },
                {
                    "key": "3",
                    "value": "Tres",
                    "subcatalog": []
                }
            ],
            "bussinessLineId" : null,
            "partnerId" : 1
        }
    """
    And request req
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications == '#array', '#present'
    And match response.notifications.[0].code == '#number', '#present', 400
    And match response.notifications.[0].message contains 'bussinessLineId - must not be null'

  Scenario: Validar que no venga nulo partnerId Crear catalogo
    Given path '/api/v1/catalogs'
    * def req =
    """
        {
            "description": "Catalog for testing purpose",
            "name": "TEST_CATALOG_TEST_MIGRACION",
            "items": [
                {
                    "key": "1",
                    "value": "Uno",
                    "subcatalog": []
                },
                {
                    "key": "2",
                    "value": "Dos",
                    "subcatalog": []
                },
                {
                    "key": "3",
                    "value": "Tres",
                    "subcatalog": []
                }
            ],
            "bussinessLineId" : 1,
            "partnerId" : null
        }
    """
    And request req
    When method post
    Then status 400
    * print "Response ok: ", response
    And match response.notifications == '#array', '#present'
    And match response.notifications.[0].code == '#number', '#present', 400
    #And match response.notifications.[0].message contains 'partnerId - must not be null'

  #Scenario: Validar un internal server error al duplicar un catalogo Crear catalogo
   # Given path '/api/v1/catalogs'
   # And def bodyRequest = read(userDir + source + 'requestBodyCatalogSuccessCreate.json')
   # And request bodyRequest
   # When method post
   # Then status 500
   # * print "Response ok: ", response
   # And match response.status == '#number', '#present', 500
   # And match response.error contains 'Internal Server Error'


  Scenario: Validar una respuesta correcta y crear catalogo y validar todo el response Crear catalogo
    Given path '/api/v1/catalogs'
    #* url 'http://localhost:3001/paco'
    * def name = new call().getNameRandom()
    * def names = new call().getNameRandom()
    * def javaMethod = new call().setName('TEST_CATALOG_'+ names +'_' + name)
    And def bodyRequest = read(userDir + source + 'requestBodyCatalogNewFile.json')
    And request bodyRequest
    When method post
    Then status 200
    * print "Response ok: ", response
    * def setId = new call().setNumberCatalog(response.data.id)
    * print "item: ", response.data.items[0].id
    * def setItem = new call().setNumberItemCatalog(response.data.items[0].id)
    And match response.data.id == '#number', '#notnull'
    And match response.data.name == '#string','#notnull'
    And match response.data.description == '#string','#notnull'
    And match response.data.businessLine == '#object'
    And match response.data.businessLine.id == '#present'
    And match response.data.businessLine.name == '#present'
    And match response.data.associated.id == '#present'
    And match response.data.associated.name == '#present'
    And match response.data.active == '#boolean', true
    And match response.data.items == '#array', '#notnull'
    And match response.data.items.[*].key == '#present'
    And match response.data.items.[*].value == '#present'
    And match response.data.items.[*].subcatalog == '#present'
    And match response.data.items.[*].id == '#present'
    And match response.data.items.[*].active == '#present'
    And match response.notifications == '#array'
    And match response.notifications.[*].code contains [200]
    And match response.notifications.[*].message contains 'Se agregó el catálogo de manera exitosa'

  Scenario: Validar un número de catalogo no registrado Find One catalog
    Given path '/api/v1/catalog/5000054151'
    When method get
    Then status 404
    * print "Response ok: ", response
    And match response.notifications == '#array', '#notnull'
    And match response.notifications.[*].message contains 'No data found'

  Scenario: Validar un número previamente creado Find One catalog
    * def id = new call().getNumberCatalog()
    Given path '/api/v1/catalog/' + id
    When method get
    Then status 200
    * print "Response ok: ", response
    And match response.data.id == '#number', '#notnull'
    And match response.data.name == '#string','#notnull'
    And match response.data.description == '#string','#notnull'
    And match response.data.businessLine == '#object'
    And match response.data.businessLine.id == '#present'
    And match response.data.businessLine.name == '#present'
    And match response.data.associated.id == '#present'
    And match response.data.associated.name == '#present'
    And match response.data.active == '#boolean', true
    And match response.data.items == '#array', '#notnull'
    And match response.data.items.[*].key == '#present'
    And match response.data.items.[*].value == '#present'
    And match response.data.items.[*].subcatalog == '#present'
    And match response.data.items.[*].id == '#present'
    And match response.data.items.[*].active == '#present'


  Scenario: Validar una respuesta correcta y validar todo el response Actualizar catalogo
    * def ID = new call().getNumberCatalog()
    * url getUrl + '/api/v1/catalog/' + ID
    * def name = new call().getNameRandom()
    * def names = new call().getNameRandom()
    * def javaMethod = new call().setNameUpdate(names +'_' + name + '_TEST_CATALOG')
    And def bodyRequest = read(userDir + source + 'requestBodyUpdateCatalog.json')
    And request bodyRequest
    When method put
    Then status 200
    * print "Response ok: ", response
    And match response.data.id == '#number', '#notnull'
    And match response.data.name == '#string','#notnull'
    And match response.data.description == '#string','#notnull'
    And match response.data.businessLine == '#object'
    And match response.data.businessLine.id == '#present'
    And match response.data.businessLine.name == '#present'
    And match response.data.associated.id == '#present'
    And match response.data.associated.name == '#present'
    And match response.data.active == '#boolean', true
    And match response.data.items == '#array', '#notnull'
    And match response.data.items.[*].key == '#present'
    And match response.data.items.[*].value == '#present'
    And match response.data.items.[*].subcatalog == '#present'
    And match response.data.items.[*].id == '#present'
    And match response.data.items.[*].active == '#present'
    And match response.notifications == '#array'
    And match response.notifications.[*].code contains [200]
    And match response.notifications.[*].message contains 'Se guardaron los cambios de manera exitosa'

  Scenario: Validar cuando no exista un id registrado Actualizar catalogo
    Given path '/api/v1/catalog/5000000'
    And def bodyRequest = read(userDir + source + 'requestBodyUpdateCatalog.json')
    And request bodyRequest
    When method put
    Then status 404
    * print "Response ok: ", response
    And match response.notifications.[0].message contains 'No data found'

  Scenario: Validar cuando no exista un id registrado borrar item de catalogo
    Given path '/api/v1/catalog/5000000/item/4'
    And def bodyRequest = read(userDir + source + 'requestBodyUpdateCatalog.json')
    And request bodyRequest
    When method put
    Then status 400

  Scenario: Validar una respuesta correcta y validar todo el response borrar Item de catalogo
    * def ID = new call().getNumberCatalog()
    * def item = new call().getNumberItemCatalog()
    Given path '/api/v1/catalog/'+ID+'/item/' + item
    And def bodyRequest =
    """
    {
    "status" : "INACTIVE"
    }
    """
    And request bodyRequest
    When method put
    Then status 200
    * print "Response ok: ", response
    And match response.data.id == '#number', '#notnull'
    And match response.data.name == '#string','#notnull'
    And match response.data.description == '#string','#notnull'
    And match response.data.businessLine == '#object'
    And match response.data.businessLine.id == '#present'
    And match response.data.businessLine.name == '#present'
    And match response.data.associated.id == '#present'
    And match response.data.associated.name == '#present'
    And match response.data.active == '#boolean'
    And match response.data.items == '#array', '#notnull'
    And match response.data.items.[*].key == '#present'
    And match response.data.items.[*].value == '#present'
    And match response.data.items.[*].subcatalog == '#present'
    And match response.data.items.[*].id == '#present'
    And match response.data.items.[*].active == '#present'
    And match response.data.items.[0].active == false
    And match response.notifications == '#array'
    And match response.notifications.[*].code contains [200]
    And match response.notifications.[*].message contains 'Se borro la opción del catalogo de manera exitosa'


  Scenario: Validar una respuesta correcta y validar todo el response borrar catalogo
    * def ID = new call().getNumberCatalog()
    Given path '/api/v1/catalog/'+ID+'/status'
    And def bodyRequest =
    """
    {
    "status" : "INACTIVE"
    }
    """
    And request bodyRequest
    When method put
    Then status 200
    * print "Response ok: ", response
    And match response.data.id == '#number', '#notnull'
    And match response.data.name == '#string','#notnull'
    And match response.data.description == '#string','#notnull'
    And match response.data.businessLine == '#object'
    And match response.data.businessLine.id == '#present'
    And match response.data.businessLine.name == '#present'
    And match response.data.associated.id == '#present'
    And match response.data.associated.name == '#present'
    And match response.data.active == '#boolean', false
    And match response.data.items == '#array', '#notnull'
    And match response.data.items.[*].key == '#present'
    And match response.data.items.[*].value == '#present'
    And match response.data.items.[*].subcatalog == '#present'
    And match response.data.items.[*].id == '#present'
    And match response.data.items.[*].active == '#present'
    And match response.notifications == '#array'
    And match response.notifications.[*].code contains [200]
    And match response.notifications.[*].message contains 'Se borro el catalogo de manera exitosa'