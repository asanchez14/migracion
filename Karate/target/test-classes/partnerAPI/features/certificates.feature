Feature: Certificados

  Background:
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_cert')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']



  Scenario: Validar una respuesta correcta con el ID de SALUD 242
    Given path '/api/v1/certificates/document/242'
    And request ''
    When method post
    Then status 200
    * print "Response code 200 ID=242 ", response

  Scenario: Validar una respuesta correcta con el ID de MOMENTOS 243
    Given path '/api/v1/certificates/document/243'
    And request ''
    When method post
    Then status 200
    * print "Response code 200 ID=243 ", response

  Scenario: Validar una respuesta correcta con el ID de PLENITUD 244
    Given path '/api/v1/certificates/document/244'
    And request ''
    When method post
    Then status 200
    * print "Response code 200 ID=244 ", response

  Scenario: Validar una respuesta correcta con el ID de VALORA 245
    Given path '/api/v1/certificates/document/245'
    And request ''
    When method post
    Then status 200
    * print "Response code 200 ID=245 ", response

  Scenario: Validar una respuesta correcta con el ID de VIDA 247
    Given path '/api/v1/certificates/document/247'
    And request ''
    When method post
    Then status 200
    * print "Response code 200 ID=247 ", response


  Scenario: Validar el error controlado de un path incorrecto
    Given path '/api/v1/certificates/documents'
    And request ''
    When method post
    Then status 404
    * print "Response: ", response

  Scenario: Validar que al momento de lanzar un ID inexistente en la BD mande un error controlado
    Given path '/api/v1/certificates/document/5000'
    And request ''
    When method post
    Then status 500
    * print "Response: ", response
