Feature: Validación de API Partner digitalization

  Background:
    ####### mock ####

    * def javaClass1 = Java.type('DAL.LoadPropertiesFile')
    * def getMockUrl = new javaClass1().getPropertiesLoad('url_mock')
    * url getMockUrl

    ############### CALL SCENARIO
    * def javaClass2 = Java.type('methodCall.CallMethodDigitization')
    * def numberPolicy = new javaClass2().getNumberOfPolicyRegisteredForDigitization()

    ############## CALL URL AWS

    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_digitalization')


    ######
    * url getUrl
    * header Accept = 'application/json'
    * header partnerId = '2',
    * header transactionId = '1'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/partnerAPI/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodsIssuancePost')

    ################################## SUCCESS RESPONSES ########################################
########## MODIFICAR MOCK ######
  Scenario: Validar una respuesta valida del servicio digitization
    * url getMockUrl
    #Given path '/api/v1/policies/'+ numberPolicy +'/documents'
    Given path '/api/v1/policies/98864243779G565/documents'
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationSuccessPUT.json')
    And request bodyRequest
    When method put
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    And match response.data == "#array", "#notnull"
    And match response.data.[*].reference == "#notnull"
    And match response.data.[*].format == "#notnull"
    And match response.data.[*].type == "#notnull"
    And match response.data.[*].status == "#notnull"
    And match response.notifications == "#array"
    And match response.notifications.[0].code == "#notnull"
    And match response.notifications.[0].message contains "Los documentos se han cargado correctamente."

    Scenario: Al no colocar un número de póliza en el path veriricar el HTTP Code 404
      Given path '/api/v1/policies//documents'
      And def bodyRequest = read(userDir + source + 'requestBodyDigitizationSuccessPUT.json')
      And request bodyRequest
      When method put
      Then status 404
      And print "Validate HTTP Code 404 response: ", response
      And match response.error contains "Not Found"

  Scenario: Al no colocar un número de póliza inexistente en base
    Given path '/api/v1/policies/123456/documents'
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationSuccessPUT.json')
    And request bodyRequest
    When method put
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications.[0].message == "Policy number not found."

  Scenario: Cuando se ingrese el formato null
    Given path '/api/v1/policies/'+ numberPolicy +'/documents'
    * def bodyRequest = new javaClass2().setFortmatNull()
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationBad.json')
    And request bodyRequest
    When method put
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications == "#array"
    And match response.notifications.[1].message contains "El formato del documento es requerido"

  Scenario: Cuando se ingrese el type null
    Given path '/api/v1/policies/'+ numberPolicy +'/documents'
    * def bodyRequest = new javaClass2().setTypeNull()
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationBad.json')
    And request bodyRequest
    When method put
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications == "#array"
    And match response.notifications.[1].message contains "El tipo de documento es requerido."

  Scenario: Cuando se ingrese el value null
    Given path '/api/v1/policies/'+ numberPolicy +'/documents'
    * def bodyRequest = new javaClass2().setValueNull()
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationBad.json')
    And request bodyRequest
    When method put
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications == "#array"
    And match response.notifications.[1].message contains "El valor del documento es requerido"

  Scenario: Cuando no se ingrese un formato registrado (PDF, JPG, PNG, BMP), genere un error
    Given path '/api/v1/policies/'+ numberPolicy +'/documents'
    * def bodyRequest = new javaClass2().setFortmat()
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationBad.json')
    And request bodyRequest
    When method put
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications == "#array"
    And match response.notifications.[1].message contains "[PDF, JPG, PNG, BMP]"

  Scenario: Cuando no se ingrese un type registrado (CHECKLIST, CONSENT, DOCUMENT_IDENTIFIER)
    Given path '/api/v1/policies/'+ numberPolicy +'/documents'
    * def bodyRequest = new javaClass2().setType()
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationBad.json')
    And request bodyRequest
    When method put
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications == "#array"
    And match response.notifications.[1].message contains "[CHECKLIST, CONSENT, DOCUMENT_IDENTIFIER]"

  Scenario: Cuando no se coloque el transactionId en el header, mande error
    Given path '/api/v1/policies/'+ numberPolicy +'/documents'
    * header transactionId = ''
    And def bodyRequest = read(userDir + source + 'requestBodyDigitizationSuccessPUT.json')
    And request bodyRequest
    When method put
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications == "#array"
    And match response.notifications.[1].message contains "El identificador de la transaccion es requerido"

  Scenario: Al no colocar un número de póliza en el path verificar el HTTP Code 404 get documents
    Given path '/api/v1/policies//documents'
    When method get
    Then status 404
    And print "Validate HTTP Code 404 response: ", response
    And match response.error contains "Not Found"

  Scenario: Al no colocar un número de póliza inexistente en base get documents
    Given path '/api/v1/policies/123456/documents'
    When method get
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications.[0].message == "Policy number not found."


  Scenario: Al no colocar un número de póliza en el path verificar el HTTP Code 404 get downloads
    Given path '/api/v1/policies//documents/163'
    When method get
    Then status 404
    And print "Validate HTTP Code 404 response: ", response
    And match response.error contains "Not Found"

  Scenario: Al no colocar un número de póliza inexistente en base get downloads
    Given path '/api/v1/policies/123456/documents/163'
    When method get
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications.[0].message == "Policy number not found."

  Scenario: Al no colocar una referencia valida, mande error controlado get downloads
    Given path '/api/v1/policies/' + numberPolicy + '/documents/1000521'
    When method get
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    And match response.notifications.[0].message == "No document found for policy number and reference."

  Scenario: Verificar una respuesta correcta del listado de documentos registrados
    #quitar los mocks
      #Given path '/api/v1/policies/'+numberPolicy+'/documents'
    Given path '/api/v1/policies/98864243779G565/documents'
    When method get
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    And match response.data == "#array", "#notnull"
    And match response.data.[*].format == "#notnull"
    And match response.data.[*].type == "#notnull"
    And match response.data.[*].reference == "#notnull"
    And match response.data.[*].status == "#notnull"
    And match response.notifications.[*].message == "#notnull"

########mock mod
  Scenario: Verificar una respuesta correcta de la descarga correcta del archivo en base64
    * url getMockUrl
    Given path '/api/v1/policies/02020043940G8552/documents/163'
    When method get
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    And match response.data == "#array", "#notnull"
    And match response.data.[*].format == "#notnull"
    And match response.data.[*].type == "#notnull"
    And match response.data.[*].reference == "#notnull"
    And match response.data.[*].value == "#notnull"
    And match response.data.[*].status == "#notnull"
    And match response.notifications.[*].message == "#notnull"
