Feature: Validación de API Partner notification

  Background:

    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_notification')


    ######
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/partnerAPI/features/request/'


    ################################## SUCCESS RESPONSES ########################################

  Scenario: Validar una respuesta valida del servicio notification
    * url getUrl
    Given path '/prueba/kafka/produces'
    And def bodyRequest = read(userDir + source + 'requestBodyNotification200.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    And match response contains 'mensaje enviado'


    ############################# bad format and bad type ########################

  Scenario: Validar que mande el archivo con mal formato
    * url getUrl
    Given path '/prueba/kafka/produces'
    And def bodyRequest = read(userDir + source + 'requestBodyNotificationBad.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    And match response contains 'mensaje enviado'

  ################################# without request ################################

  Scenario: Validar que cuando no se mande el request o esté vacío mande un status 400
    * url getUrl
    Given path '/prueba/kafka/produces'
    And request ''
    When method post
    Then status 415
    And print "Validate HTTP Code 415 response: ", response
