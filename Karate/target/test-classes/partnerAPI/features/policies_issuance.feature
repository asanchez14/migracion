Feature: Validación de API Partner policies/issuance HTTP POST

  Background:
    * def javaClass = Java.type('DAL.LoadPropertiesFile')
    * def getUrl = new javaClass().getPropertiesLoad('url_issuance_subscribe')
    * url getUrl
    * header Accept = 'application/json'
    * def userDir = karate.properties['user.dir']
    * def source = '/src/test/java/partnerAPI/features/request/'
    * def javaClass = Java.type('methodCall.CallMethodsIssuancePost')

    ################################## SUCCESS RESPONSES ########################################

  Scenario: Validar una respuesta valida del servicio issuance Scotiabank
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBodySBKVenta.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"



  Scenario: Validar una respuesta valida del servicio issuance Scotiabank Salud
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBodySBKSalud.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"



  Scenario: Validar una respuesta valida del servicio issuance Volkswagen
    Given path '/api/v1/policies/issuance'
    And def bodyRequest = read(userDir + source + 'requestBodyVW.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"

  Scenario: Validar una respuesta valida del servicio issuance Bancoppel
    Given path '/api/v1/policies/issuance'
    * def javaMethod = new javaClass().setPolicyHolderGivenNameRandomName()
    And def bodyRequest = read(userDir + source + 'requestBodyBancoppel.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"





  Scenario: Validar una respuesta valida del servicio issuance Scotiabank GAP
    Given path '/api/v1/policies/issuance'
    * def javaMethod = new javaClass().setPolicyHolderGivenNameRandomName()
    And def bodyRequest = read(userDir + source + 'requestBodyGAPScotiabank.json')
    And request bodyRequest
    When method post
    Then status 200
    And print "Validate HTTP Code 200 response GAP: ", response
    Then match response == "#object"
    And match response.pathResult == "#string", "#present"
    And match response.policyInfo == "#object", "#present"
    And match response.policyInfo.uuid == "#string", "#present"
    And match response.policyInfo.policyNumber == "#string", "#present"
    And match response.policyInfo.policyStatus == "#string", "#present"
    And match response.policyInfo.policyEffectiveDate == "#string", "#present"
    And match response.policyInfo.policyExpirationDate == "#string", "#present"
    And match response.policyInfo.policyPeriod == "#string", "#present"

    ################################ validations #######################################




  Scenario: Verificar que no permita pasar la Póliza al no contar con un mes correcto en la fecha
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPolicySubjectInsuredSubjectDetailBirthLong()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza cuando no tenga la edad mínima
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPolicySubjectInsuredSubjectDetailAgeMin()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"


  Scenario: Verificar que no permita pasar la Póliza cuando no tenga la edad mínima
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPolicySubjectInsuredSubjectDetailAgeMax()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza al no ingresar un birthCountryId conforme al catálogo
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPolicySubjectInsuredSubjectDetailCatalogCountry()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza al no ingresar un personType correcto (ASEGURADO, CONTRATANTE, TITULAR)
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPolicySubjectInsuredSubjectDetailPersonType()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza al no cumplir con el porcentaje requerido que es 100%
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setPolicySubjectInsuredSubjectDetailPercent()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza cuando el valor canalId no est+e registrado en catálogo
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setCanalId()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza cuando el valor notificationId no esté registrado en catálogo
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    * def javaMethod = new javaClass().setNotificationId()
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza cuando el valor medioId no esté registrado en catálogo
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    And def bodyRequest = read(userDir + source + 'requestBodyBad.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"

  Scenario: Verificar que no permita pasar la Póliza cuando tenga más de 10 beneficiarios y sólo acepte (ESPOSO, ESPOSA, CONYUGUE, HIJO, HIJA)
    Given path '/api/v1/policies/issuance'
    * def sleep = function(millis){ java.lang.Thread.sleep(millis) }
    * eval sleep(2000)
    And def bodyRequest = read(userDir + source + 'requestBodyBeneficiary.json')
    And request bodyRequest
    When method post
    Then status 400
    And print "Validate HTTP Code 400 response: ", response
    Then match response == "#array"
    And match response.[0].errorCode == "#number"
    And match response.[0].errorMessages == "#array", "#present"







