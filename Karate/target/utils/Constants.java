package utils;

public class Constants {

    public static final String PATH_SALES_REQUEST_JSON = "/src/test/java/Sales/features/requestJSON/";
    public static final String PATH_PARTNER_REQUEST_JSON = "/src/test/java/partnerAPI/features/request/";
    public static final String PATH_ISSUANSE_REQUEST_JSON = "/src/test/java/partnerAPI/features/request/";
    public static final String PATH_EFFICACY_RETRIEVE_REQUEST_JSON = "/src/test/java/Bancoppel/features/request/";
    public static final String PATH_CANCEL_REQUEST_JSON = "/src/test/java/Bancoppel/features/request/";
    public static final String PATH_UPDATE_REQUEST_JSON = "/src/test/java/Bancoppel/features/request/";
    public static final String PATH_RENEWAL_REQUEST_JSON = "/src/test/java/Bancoppel/features/request/";
    public static final String USER_DIR = "user.dir";
    public static final String POLICY_OBJECT = "policy";
    public static final String NUMBER = "number";
    public static final String PATH_NAME = "target";
    public static final String DATA_TYPE = "json";

    public static final String PN_SALES_POLICIES_POST_SBK_VIDA = "sale_policies_post_sbkVida";
    public static final String PN_SALES_POLICIES_POST_SBK ="sale_policies_post_sbk";
    public static final String PN_SALES_POLICIES_POST_BANCOPPEL = "sale_policies_post_bancoppel";
    public static final String PN_SALES_POLICIES_POST_VW = "sale_policies_post_vw";
    public static final String PN_ISSUANCE_POLICIES_POST_SBK ="issuance_policies_post";


    ///////features classpath
    public static final String SALES_POLICIES_SBK_VIDA_POST_PATH = "classpath:Sales/features/sales_policies_post_sbkvida.feature";
    public static final String SALES_POLICIES_SBK_POST_PATH = "classpath:Sales/features/sales_policies_post_sbk.feature";
    public static final String SALES_POLICIES_BANCOPPEL_POST_PATH = "classpath:Sales/features/sales_policies_post_bancoppel.feature";
    public static final String SALES_POLICIES_VW_POST_PATH = "classpath:Sales/features/sales_policies_post_vw.feature";

    //
    public static final String ISSUANCE_POLICIES_SBK_POST_PATH = "classpath:partnerAPI/features/policies_issuance_post_sbk.feature.feature";

    // Features bancoppel //
    public static final String RETRIEVE_POLICIES_POST = "classpath:Bancoppel/features/retrieve_policies_post_bancoppel.feature";
    public static final String UPDATE_POLICIES = "classpath:Bancoppel/features/update_policies.feature";
    public static final String ISSUANCE_REQUEST_POLICIES = "classpath:Bancoppel/features/issuance_bancoppel.feature";
    public static final String EFFICACY_POLICIES_POST = "classpath:Bancoppel/features/efficacy_retrieve_policies_bancoppel.feature";
    public static final String CANCEL_POLICIES_POST = "classpath:Bancoppel/features/cancel_revers_bancoppel.feature";
    public static final String RENEWAL_POLICIES_POST = "classpath:Bancoppel/features/renovaciones_policies_bancoppel.feature";

}
